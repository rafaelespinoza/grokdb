package executor

import (
	"fmt"
	"io"

	"bitbucket.org/rafaelespinoza/grokdb"
)

// Selection yields only records which return true for a predicate function.
type Selection struct {
	Input []grokdb.Iterator
	Predicate
	cursor int
	tuples []grokdb.Tuple
}

func InitSelection(i []grokdb.Iterator, p Predicate) *Selection {
	return &Selection{i, p, 0, make([]grokdb.Tuple, 0)}
}

func (s *Selection) Next() (grokdb.Tuple, error) {
	input := s.Input[0]

	for tup, err := input.Next(); err != io.EOF; tup, err = input.Next() {
		if err != nil {
			return nil, err
		}
		s.cursor++

		if s.Predicate(tup) {
			s.tuples = append(s.tuples, tup)
			return tup, nil
		}
	}

	return nil, io.EOF
}

func (s Selection) Reset() error {
	var err error

	for _, iter := range s.Input {
		err = iter.Reset()
		if err != nil {
			return err
		}
	}

	s.cursor = 0
	s.tuples = make([]grokdb.Tuple, 0)

	return nil
}

func (s Selection) Close() error {
	fmt.Println("Selection done")
	return nil
}

type Predicate func(grokdb.Tuple) bool

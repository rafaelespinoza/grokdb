package executor

import (
	"fmt"
	"io"

	"bitbucket.org/rafaelespinoza/grokdb"
)

// Average is an aggregator node that yields the average value of its input.
type Average struct {
	Input     []grokdb.Iterator
	avgBy     Column
	groupBy   Column
	nextTuple grokdb.Tuple
	attrs     []grokdb.Attribute
}

func InitAverage(input []grokdb.Iterator, groupBy Column, avgBy Column) (*Average, error) {
	if len(input) < 1 {
		return nil, fmt.Errorf("Not enough input")
	}
	// read first record to determine current group
	tup, err := input[0].Next()
	if err == io.EOF {
		tup = nil
	} else if err != nil {
		return nil, err
	}

	attrs := []grokdb.Attribute{grokdb.Attribute{Name: groupBy.Name, Type: groupBy.Type}}
	if !groupBy.Equals(&avgBy) {
		attrs = append(attrs, grokdb.Attribute{Name: avgBy.Name, Type: avgBy.Type})
	}

	avg := Average{
		Input:     input,
		avgBy:     avgBy,
		groupBy:   groupBy,
		nextTuple: tup,
		attrs:     attrs,
	}
	return &avg, nil
}

func (a *Average) Next() (grokdb.Tuple, error) {
	if a.nextTuple == nil {
		return nil, io.EOF
	}

	sum := a.avgBy.mustGetFloat(a.nextTuple)
	currentGrouping := NewGrouping(a.groupBy, a.nextTuple)

	count := 1
	input := a.Input[0]
	a.nextTuple = nil

	for tup, err := input.Next(); err != io.EOF; tup, err = input.Next() {
		if err != nil {
			return nil, err
		}
		sameGroup := a.groupBy.mustCompare(tup, currentGrouping.value)

		if !sameGroup {
			a.nextTuple = tup
			break
		}

		sum += a.avgBy.mustGetFloat(tup)
		count += 1
	}

	avgish := sum / float64(count)
	rounded := roundToNearest(avgish, 0.05)
	if len(a.attrs) == 1 {
		return grokdb.Tuple([]interface{}{rounded}), nil
	}
	return grokdb.Tuple([]interface{}{currentGrouping.value, rounded}), nil
}

func (a *Average) Reset() error {
	var err error

	for _, iter := range a.Input {
		err = iter.Reset()
		if err != nil {
			return err
		}
	}

	a.attrs = make([]grokdb.Attribute, 0)
	a.nextTuple = nil

	return nil
}

func (a Average) Close() error {
	fmt.Println("Average done")
	return nil
}

package executor

import (
	"fmt"
	"hash/fnv"
	"io"
	"strconv"

	"bitbucket.org/rafaelespinoza/grokdb"
)

type HashTable map[interface{}][]grokdb.Tuple

func newHashTable() HashTable { return make(map[interface{}][]grokdb.Tuple) }

type hashPartitions map[string][]grokdb.Tuple

func newHashPartitions() hashPartitions {
	return hashPartitions(make(map[string][]grokdb.Tuple))
}

func initHashPartitions(input grokdb.Iterator, typ grokdb.Type, col, buckets int) (*hashPartitions, error) {
	parts := newHashPartitions()

	for tup, err := input.Next(); err != io.EOF; tup, err = input.Next() {
		if err != nil {
			return nil, err
		}
		key, err := hashValue(tup[col], typ, buckets)
		if err != nil {
			// just let it go
			fmt.Printf("Warning: %v\n", err)
			continue
		}

		if tups, ok := parts[key]; !ok {
			parts[key] = []grokdb.Tuple{tup}
		} else {
			parts[key] = append(tups, tup)
		}
	}

	if err := input.Reset(); err != nil {
		return nil, err
	}

	return &parts, nil
}

func hashValue(val interface{}, typ grokdb.Type, buckets int) (string, error) {
	switch typ {
	case grokdb.Int:
		input, ok := val.(int)
		if !ok {
			return "", fmt.Errorf("Value %v is not an int", val)
		}
		return hashInt(input, buckets), nil
	case grokdb.Float:
		input, ok := val.(float64)
		if !ok {
			return "", fmt.Errorf("Value %v is not a float64", val)
		}
		return hashFloat(input, buckets), nil
	case grokdb.String:
		input, ok := val.(string)
		if !ok {
			return "", fmt.Errorf("Value %v is not a string", val)
		}
		return hashString(input, buckets), nil
	default:
		err := fmt.Errorf("Cannot handle type %v", typ)
		return "", err
	}
}

func hashInt(input, n int) string {
	mod := input % n
	return strconv.Itoa(mod)
}

func hashFloat(input float64, n int) string {
	char := int(input) // characteristic, digits the the left of the '.' radix point

	var mant float64 // mantissa, digits to the right of the '.' radix point
	if char < 0 {
		mant = input + float64(char)
	} else {
		mant = input - float64(char)
	}
	mant = roundToNearest(mant, acceptableFloatingPointEpsilon) * 100
	sum := char + int(mant)
	return hashInt(sum, n)
}

func hashString(input string, n int) string {
	hasher := fnv.New32a()
	byts := make([]byte, len(input))
	for i, ch := range input {
		byts[i] = byte(ch)
	}
	hasher.Write(byts)
	val := hasher.Sum(nil)
	sum := sumBytes(val)
	return hashInt(sum, n)
}

func sumBytes(byts []byte) int {
	sum := 0
	for _, b := range byts {
		sum += int(b)
	}
	return sum
}

package executor

import (
	"fmt"
	"io"
	"strings"

	"bitbucket.org/rafaelespinoza/grokdb"
)

// Distinct drops records in the stream that are the same as the prior one.
type Distinct struct {
	Input         []grokdb.Iterator
	ColumnIndices []int
	lastUniq      string
}

func InitDistinct(input []grokdb.Iterator, columnIndices []int) *Distinct {
	ci := make([]int, len(columnIndices))
	copy(ci, columnIndices)
	return &Distinct{input, ci, ""}
}

func (d *Distinct) Next() (grokdb.Tuple, error) {
	input := d.Input[0]

	for tup, err := input.Next(); err != io.EOF; tup, err = input.Next() {
		if err != nil {
			return nil, err
		}
		h := d.hash(tup)
		if h != d.lastUniq {
			d.lastUniq = h // this is new
			return tup, nil
		}
	}

	return nil, io.EOF
}

func (d *Distinct) Reset() error {
	var err error

	for _, iter := range d.Input {
		err = iter.Reset()
		if err != nil {
			return err
		}
	}

	d.lastUniq = ""

	return nil
}

func (d Distinct) hash(tup grokdb.Tuple) string {
	h := make([]string, len(d.ColumnIndices))
	for i, colInd := range d.ColumnIndices {
		val := tup[colInd]
		h[i] = fmt.Sprintf("%s", val)
	}
	return strings.Join(h, ".")
}

func (d Distinct) Close() error {
	fmt.Println("Distinct done")
	return nil
}

package executor

import (
	"fmt"
	"io"
	"reflect"
	"testing"

	"bitbucket.org/rafaelespinoza/grokdb"
)

func TestNestedLoopsJoin(t *testing.T) {
	// EXPLAIN VERBOSE
	// SELECT m.title, m.year, r.rating
	// FROM movies m, ratings r
	// WHERE m.id = r.movie_id
	//   AND m.title = 'Jumanji';

	movies := grokdb.Relation{
		RelationHeader: grokdb.RelationHeader{
			Name: "movies",
			Attributes: []grokdb.Attribute{
				grokdb.Attribute{Name: "id", Type: grokdb.Int},
				grokdb.Attribute{Name: "title", Type: grokdb.String},
				grokdb.Attribute{Name: "year", Type: grokdb.Int},
			},
		},
		Tuples: []grokdb.Tuple{
			grokdb.Tuple([]interface{}{5000, "Jumanji", 1995}),
			grokdb.Tuple([]interface{}{5050, "Toy Story", 1995}),
			grokdb.Tuple([]interface{}{7080, "Jumanji", 2017}),
		},
	}
	ratings := grokdb.Relation{
		RelationHeader: grokdb.RelationHeader{
			Name: "ratings",
			Attributes: []grokdb.Attribute{
				grokdb.Attribute{Name: "userId", Type: grokdb.Int},
				grokdb.Attribute{Name: "movieId", Type: grokdb.Int},
				grokdb.Attribute{Name: "rating", Type: grokdb.Float},
			},
		},
		Tuples: []grokdb.Tuple{
			grokdb.Tuple([]interface{}{1, 4000, 5}),
			grokdb.Tuple([]interface{}{1, 5000, 3}),
			grokdb.Tuple([]interface{}{3, 5000, 4}),
			grokdb.Tuple([]interface{}{2, 5050, 4}),
			grokdb.Tuple([]interface{}{3, 5050, 2}),
		},
	}

	moviesSel := InitSelection(
		[]grokdb.Iterator{InitInMemSeqScan(movies.RelationHeader, movies.Tuples)},
		func(t grokdb.Tuple) bool { return t[1] == "Jumanji" },
	)
	moviesProj := InitProjection(
		[]grokdb.Iterator{moviesSel},
		[]int{0, 1, 2},
	)
	ratingsProj := InitProjection(
		[]grokdb.Iterator{InitInMemSeqScan(ratings.RelationHeader, ratings.Tuples)},
		[]int{0, 1, 2},
	)

	join, err := InitNestedLoopJoin(
		[]joinRelation{
			joinRelation{
				Iterator: moviesProj,
				colInd:   []int{1, 2},
				joinOn:   Column{}, // TODO: use
			},
			joinRelation{
				Iterator: ratingsProj,
				colInd:   []int{2},
				joinOn:   Column{}, // TODO: use
			},
		},
		func(m, r grokdb.Tuple) bool { return m[0] == r[1] }, // movies.id == ratings.movieId
	)
	if err != nil {
		panic(err)
	}
	actual := make([]grokdb.Tuple, 0)
	for tup, err := join.Next(); err != io.EOF; tup, err = join.Next() {
		if err != nil {
			panic(err)
		}
		actual = append(actual, tup)
	}

	expected := []grokdb.Tuple{
		grokdb.Tuple([]interface{}{"Jumanji", 1995, 3}),
		grokdb.Tuple([]interface{}{"Jumanji", 1995, 4}),
	}
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("Actual tuples not equal to expected tuples.\n")
		fmt.Printf("Got: 		%+v\n", actual)
		fmt.Printf("Expected: 	%+v\n", expected)
	}
}

func TestSortMergeJoin(t *testing.T) {
	moviesHeader := grokdb.RelationHeader{
		Name: "movies",
		Attributes: []grokdb.Attribute{
			grokdb.Attribute{Name: "movieId", Type: grokdb.Int},
			grokdb.Attribute{Name: "title", Type: grokdb.String},
			grokdb.Attribute{Name: "year", Type: grokdb.Int},
		},
	}
	ratingsHeader := grokdb.RelationHeader{
		Name: "ratings",
		Attributes: []grokdb.Attribute{
			grokdb.Attribute{Name: "userId", Type: grokdb.Int},
			grokdb.Attribute{Name: "movieId", Type: grokdb.Int},
			grokdb.Attribute{Name: "rating", Type: grokdb.Float},
			grokdb.Attribute{Name: "timestamp", Type: grokdb.Int},
		},
	}
	movieScan, err := InitFilescan("sampledata-movies.csv", moviesHeader)
	if err != nil {
		t.Errorf("Error setting up test: %v", err)
		return
	}
	ratingScan, err := InitFilescan("sampledata-ratings.csv", ratingsHeader)
	if err != nil {
		t.Errorf("Error setting up test: %v", err)
		return
	}
	join, err := InitSortMergeJoin(
		[]joinRelation{
			joinRelation{
				Iterator: movieScan,
				colInd:   []int{1, 2, 0},
				joinOn:   newColumn("id", grokdb.Int, 0),
			},
			joinRelation{
				Iterator: ratingScan,
				colInd:   []int{1, 2},
				joinOn:   newColumn("movieId", grokdb.Int, 1),
			},
		},
		SortBy{newColumn("id", grokdb.Int, 0), false},
		SortBy{newColumn("movieId", grokdb.Int, 1), false},
		func(a, b grokdb.Tuple) bool { return a[0] == b[1] },
		func(a, b grokdb.Tuple) bool {
			valA := a[0].(int)
			valB := b[1].(int)
			return valA < valB
		},
	)
	if err != nil {
		t.Errorf("Error initializing join: %v", err)
		return
	}

	actual := make([]grokdb.Tuple, 0)
	for tup, err := join.Next(); err != io.EOF; tup, err = join.Next() {
		if err != nil {
			panic(err)
		}
		actual = append(actual, tup)
	}
}

func TestClassicHashJoin(t *testing.T) {
	// EXPLAIN VERBOSE
	// SELECT m.title, m.year, r.rating
	// FROM movies m, ratings r
	// WHERE m.id = r.movie_id
	//   AND m.title = 'Jumanji';

	movies := grokdb.Relation{
		RelationHeader: grokdb.RelationHeader{
			Name: "movies",
			Attributes: []grokdb.Attribute{
				grokdb.Attribute{Name: "id", Type: grokdb.Int},
				grokdb.Attribute{Name: "title", Type: grokdb.String},
				grokdb.Attribute{Name: "year", Type: grokdb.Int},
			},
		},
		Tuples: []grokdb.Tuple{
			grokdb.Tuple([]interface{}{5000, "Jumanji", 1995}),
			grokdb.Tuple([]interface{}{5050, "Toy Story", 1995}),
			grokdb.Tuple([]interface{}{7080, "Jumanji", 2017}),
		},
	}
	ratings := grokdb.Relation{
		RelationHeader: grokdb.RelationHeader{
			Name: "ratings",
			Attributes: []grokdb.Attribute{
				grokdb.Attribute{Name: "userId", Type: grokdb.Int},
				grokdb.Attribute{Name: "movieId", Type: grokdb.Int},
				grokdb.Attribute{Name: "rating", Type: grokdb.Float},
			},
		},
		Tuples: []grokdb.Tuple{
			grokdb.Tuple([]interface{}{1, 4000, 5}),
			grokdb.Tuple([]interface{}{1, 5000, 3}),
			grokdb.Tuple([]interface{}{3, 5000, 4}),
			grokdb.Tuple([]interface{}{2, 5050, 4}),
			grokdb.Tuple([]interface{}{3, 5050, 2}),
		},
	}

	moviesSel := InitSelection(
		[]grokdb.Iterator{InitInMemSeqScan(movies.RelationHeader, movies.Tuples)},
		func(t grokdb.Tuple) bool { return t[1] == "Jumanji" },
	)
	moviesProj := InitProjection(
		[]grokdb.Iterator{moviesSel},
		[]int{0, 1, 2},
	)
	ratingsProj := InitProjection(
		[]grokdb.Iterator{InitInMemSeqScan(ratings.RelationHeader, ratings.Tuples)},
		[]int{0, 1, 2},
	)

	join, err := InitClassicHashJoin(
		[]joinRelation{
			joinRelation{
				Iterator: moviesProj,
				colInd:   []int{1, 2},
				joinOn:   newColumn("movies.id", grokdb.Int, 0),
			},
			joinRelation{
				Iterator: ratingsProj,
				colInd:   []int{2},
				joinOn:   newColumn("ratings.movieId", grokdb.Int, 1),
			},
		},
	)
	if err != nil {
		panic(err)
	}
	actual := make([]grokdb.Tuple, 0)
	for tup, err := join.Next(); err != io.EOF; tup, err = join.Next() {
		if err != nil {
			panic(err)
		}
		actual = append(actual, tup)
	}

	expected := []grokdb.Tuple{
		grokdb.Tuple([]interface{}{"Jumanji", 1995, 3}),
		grokdb.Tuple([]interface{}{"Jumanji", 1995, 4}),
	}
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("Actual tuples not equal to expected tuples.\n")
		fmt.Printf("Got: 		%+v\n", actual)
		fmt.Printf("Expected: 	%+v\n", expected)
	}
}

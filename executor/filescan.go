package executor

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"os"
	"strconv"

	"bitbucket.org/rafaelespinoza/grokdb"
)

// Filescan yields each row for the table as needed. Only accepts CSV format.
type Filescan struct {
	header   grokdb.RelationHeader
	filename string
	reader   *csv.Reader
	line     int
}

func InitFilescan(filename string, header grokdb.RelationHeader) (*Filescan, error) {
	var filescan Filescan
	filescan.filename = filename
	filescan.line = 0

	file, err := os.Open(filename)

	if err != nil {
		return &filescan, err
	}

	br := bufio.NewReader(file)
	filescan.reader = csv.NewReader(br)

	// Assume first line is headers. This sets up next Read to first data line.
	err = filescan.setHeader(header)

	if err != nil {
		return &filescan, err
	}

	return &filescan, err
}

func (f *Filescan) setHeader(header grokdb.RelationHeader) error {
	cols, err := f.reader.Read()
	if err != nil {
		return err
	} else if len(cols) != len(header.Attributes) {
		lengthErr := fmt.Errorf("Number of columns in file (%d) does not match number of attributes in headers (%d)", len(cols), len(header.Attributes))
		return lengthErr
	}

	for i, col := range cols {
		if col != header.Attributes[i].Name {
			nameErr := fmt.Errorf("CSV column name (%s) does not match header attribute name (%s)", col, header.Attributes[i].Name)
			return nameErr
		}
	}

	f.header = header
	return nil
}

func (f *Filescan) Next() (grokdb.Tuple, error) {
	rec, err := f.reader.Read()
	if err != nil {
		// In cases where record has an unexpected num of fields, the library
		// returns a record along with the error (csv.ErrFieldCount). But for
		// simplicity sake we're ignoring the incomplete tuple row.
		return nil, err
	}
	f.line++
	tup, err := f.parseRecord(f.header.Attributes, rec)
	if err != nil {
		return nil, err
	}
	return tup, nil
}

func (f Filescan) parseRecord(attrs []grokdb.Attribute, rec []string) (grokdb.Tuple, error) {
	tup := make(grokdb.Tuple, len(attrs))

	for i, attr := range attrs {
		val, err := f.parseValue(attr.Type, rec[i])
		if err != nil {
			return nil, err
		}
		tup[i] = val
	}

	return tup, nil
}

func (f Filescan) parseValue(attrType grokdb.Type, recVal string) (interface{}, error) {
	switch attrType {
	case grokdb.String:
		return recVal, nil
	case grokdb.Bool:
		val, err := strconv.ParseBool(recVal)
		if err != nil {
			return nil, err
		}
		return val, nil
	case grokdb.Int:
		val, err := strconv.Atoi(recVal)
		if err != nil {
			return nil, err
		}
		return val, nil
	case grokdb.Float:
		val, err := strconv.ParseFloat(recVal, 64)
		if err != nil {
			return nil, err
		}
		return val, nil
	default:
		err := fmt.Errorf("Could not handle type %v", attrType)
		return nil, err
	}
}
func (f *Filescan) Reset() error {
	f.line = 0

	return nil
}

func (f Filescan) Close() error {
	fmt.Println("Filescan done")
	return nil
}

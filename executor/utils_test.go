package executor

import (
	"fmt"
	"math"
	"reflect"
	"testing"

	"bitbucket.org/rafaelespinoza/grokdb"
)

func TestMapCombineTuples(t *testing.T) {
	tup1 := grokdb.Tuple([]interface{}{"alpha", "bravo", "charlie", "delta"})
	tup2 := grokdb.Tuple([]interface{}{"echo", "foxtrot", "golf", "hotel"})
	cols1 := []int{1, 3}
	cols2 := []int{0, 2}

	actual, err := mapCombineTuples(tup1, tup2, cols1, cols2)
	if err != nil {
		t.Errorf("Did not expect an error, but got one: %v", err)
	}
	expected := grokdb.Tuple([]interface{}{"bravo", "delta", "echo", "golf"})
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("Actual tuple did not equal expected tuple")
		fmt.Printf("actual:		 	%v\n", actual)
		fmt.Printf("expected: 		%v\n", expected)
	}
}

func TestRoundToNearest(t *testing.T) {
	tests := []struct {
		f, n, expected float64
	}{
		{0.01, 0.1, 0.00},
		{0.00, 0.1, 0.00},
		{-1.00, 0.01, -1.00},
		{1.00, 0.01, 1.00},
	}

	for i, test := range tests {
		out := roundToNearest(test.f, test.n)

		if math.Abs(out-test.expected) > acceptableFloatingPointEpsilon {
			t.Errorf("output %f not equal(ish) to expected %f test case: %d", out, test.expected, i)
		}
	}
}

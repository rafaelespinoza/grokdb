package executor

import (
	"fmt"
	"io"
	"reflect"
	"testing"

	"bitbucket.org/rafaelespinoza/grokdb"
)

func TestSelectFromMoviesWhereX(t *testing.T) {
	// SELECT id, title, year FROM movies WHERE title = 'Jumanji';
	header := grokdb.RelationHeader{
		Name: "testdata",
		Attributes: []grokdb.Attribute{
			grokdb.Attribute{Name: "id", Type: grokdb.Int},
			grokdb.Attribute{Name: "title", Type: grokdb.String},
			grokdb.Attribute{Name: "year", Type: grokdb.Int},
		},
	}
	rows := []grokdb.Tuple{
		grokdb.Tuple([]interface{}{5000, "Jumanji", 1995}),
		grokdb.Tuple([]interface{}{5050, "Toy Story", 1995}),
		grokdb.Tuple([]interface{}{7080, "Jumanji", 2017}),
	}

	sel := InitSelection(
		[]grokdb.Iterator{InitInMemSeqScan(header, rows)},
		func(t grokdb.Tuple) bool { return t[1] == "Jumanji" },
	)
	proj := *InitProjection(
		[]grokdb.Iterator{sel},
		[]int{0, 1, 2},
	)
	results := make([]grokdb.Tuple, 0)

	for tup, err := proj.Next(); err != io.EOF; tup, err = proj.Next() {
		if err != nil {
			panic(err)
		}
		if tup == nil {
			continue
		}
		results = append(results, tup)
	}

	expectedResults := []grokdb.Tuple{
		grokdb.Tuple([]interface{}{5000, "Jumanji", 1995}),
		grokdb.Tuple([]interface{}{7080, "Jumanji", 2017}),
	}

	if len(results) != len(expectedResults) {
		t.Errorf("Number of records incorrect. Got %d, expected %d\n", len(results), len(expectedResults))
	}

	if !reflect.DeepEqual(results, expectedResults) {
		t.Errorf("Actual tuples not to equal expected tuples.\n")
		fmt.Printf("Got: 		%+v\n", results)
		fmt.Printf("Expected: 	%+v\n", expectedResults)
	}
}

func TestSelectAvgRatingFromMoviesWhereX(t *testing.T) {
	// SELECT AVG(rating) FROM ratings WHERE movie_id = 5000;
	header := grokdb.RelationHeader{
		Name: "testdata",
		Attributes: []grokdb.Attribute{
			grokdb.Attribute{Name: "userId", Type: grokdb.Int},
			grokdb.Attribute{Name: "movieId", Type: grokdb.Int},
			grokdb.Attribute{Name: "rating", Type: grokdb.Int},
			grokdb.Attribute{Name: "timestamp", Type: grokdb.Int},
		},
	}
	rows := []grokdb.Tuple{
		grokdb.Tuple([]interface{}{1024, 4000, 5, 1112248602}),
		grokdb.Tuple([]interface{}{4096, 4000, 3, 1094785734}),
		grokdb.Tuple([]interface{}{2048, 5000, 4, 1112484580}),
		grokdb.Tuple([]interface{}{8192, 5000, 2, 1112484827}),
	}

	sel := InitSelection(
		[]grokdb.Iterator{InitInMemSeqScan(header, rows)},
		func(t grokdb.Tuple) bool { return t[1] == 5000 },
	)
	proj := InitProjection(
		[]grokdb.Iterator{sel},
		[]int{1, 2},
	)
	avg, err := InitAverage(
		[]grokdb.Iterator{proj},
		Column{
			Attribute: grokdb.Attribute{Type: grokdb.Int, Name: "movieId"},
			Index:     0,
		},
		Column{
			Attribute: grokdb.Attribute{Type: grokdb.Int, Name: "rating"},
			Index:     1,
		},
	)
	if err != nil {
		t.Errorf("Not expecting an error while initializing Average")
	}

	actual, err := (*avg).Next()
	if err != nil {
		t.Errorf("Not expecting an error %v", err)
	}
	expected := grokdb.Tuple([]interface{}{5000, 3.0})
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("Got wrong result. Got %T %v. Expected %v\n", actual, actual, expected)
	}
}

func TestSelectFromMoviesOrderByIdLimitX(t *testing.T) {
	// SELECT * FROM movies ORDER BY id LIMIT(3);
	header := grokdb.RelationHeader{
		Name: "testdata",
		Attributes: []grokdb.Attribute{
			grokdb.Attribute{Name: "id", Type: grokdb.Int},
			grokdb.Attribute{Name: "title", Type: grokdb.String},
			grokdb.Attribute{Name: "year", Type: grokdb.Int},
		},
	}
	rows := []grokdb.Tuple{
		grokdb.Tuple([]interface{}{6, "Wayne's World", 1992}),
		grokdb.Tuple([]interface{}{1, "The Godfather", 1972}),
		grokdb.Tuple([]interface{}{3, "Memento", 2000}),
		grokdb.Tuple([]interface{}{2, "Halloween", 1978}),
		grokdb.Tuple([]interface{}{5, "Enter the Dragon", 1973}),
		grokdb.Tuple([]interface{}{4, "Citizen Kane", 1941}),
	}

	sort := InitSort(
		[]grokdb.Iterator{InitInMemSeqScan(header, rows)},
		SortBy{
			Column:     newColumn("id", grokdb.Int, 0),
			descending: false,
		},
	)
	lim := *InitLimit([]grokdb.Iterator{sort}, 3)
	actual := make([]grokdb.Tuple, 0)

	for tup, err := lim.Next(); err != io.EOF; tup, err = lim.Next() {
		if err != nil {
			panic(err)
		}
		actual = append(actual, tup)
	}
	expected := []grokdb.Tuple{
		grokdb.Tuple([]interface{}{1, "The Godfather", 1972}),
		grokdb.Tuple([]interface{}{2, "Halloween", 1978}),
		grokdb.Tuple([]interface{}{3, "Memento", 2000}),
	}

	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("Got wrong result. Got %f. Expected %f\n", actual, expected)
	}
}

func TestSelectNumberOfMoviesRated(t *testing.T) {
	// SELECT COUNT(DISTINCT(movieId)) FROM ratings;
	header := grokdb.RelationHeader{
		Name: "testdata",
		Attributes: []grokdb.Attribute{
			grokdb.Attribute{Name: "userId", Type: grokdb.Int},
			grokdb.Attribute{Name: "movieId", Type: grokdb.Int},
			grokdb.Attribute{Name: "rating", Type: grokdb.Int},
			grokdb.Attribute{Name: "timestamp", Type: grokdb.Int},
		},
	}
	rows := []grokdb.Tuple{
		grokdb.Tuple([]interface{}{1024, 4000, 5, 1112248602}),
		grokdb.Tuple([]interface{}{2048, 5000, 4, 1112484580}),
		grokdb.Tuple([]interface{}{4096, 4000, 3, 1094785734}),
		grokdb.Tuple([]interface{}{8192, 5000, 2, 1112484827}),
	}

	proj := InitProjection(
		[]grokdb.Iterator{InitInMemSeqScan(header, rows)},
		[]int{1},
	)
	sort := InitSort(
		[]grokdb.Iterator{proj},
		SortBy{
			Column:     newColumn("movieId", grokdb.Int, 0),
			descending: false,
		},
	)
	distinct := InitDistinct(
		[]grokdb.Iterator{sort},
		[]int{0},
	)
	count := *InitCount([]grokdb.Iterator{distinct})

	actual, err := count.Next()
	if err != nil && err != io.EOF {
		t.Errorf("Unexpected error. %v", err)
	}
	expected := grokdb.Tuple([]interface{}{2})
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("Got wrong result. Got %v. Expected %v\n", actual, expected)
	}
}

// TODO: figure out why results are inconsistent. Sometimes [[10, 3.75]],
// sometimes [[50, 3.75]], [[30, 3.75]] probably aggregating incorrectly.
func TestSelectTopRated3Movies(t *testing.T) {
	// EXPLAIN VERBOSE
	// SELECT movie_id, AVG(rating)
	// FROM ratings
	// GROUP BY (movie_id)
	// ORDER BY AVG(rating) DESC
	// LIMIT(3);

	header := grokdb.RelationHeader{
		Name: "testdata",
		Attributes: []grokdb.Attribute{
			grokdb.Attribute{Name: "userId", Type: grokdb.Int},
			grokdb.Attribute{Name: "movieId", Type: grokdb.Int},
			grokdb.Attribute{Name: "rating", Type: grokdb.Float},
			grokdb.Attribute{Name: "timestamp", Type: grokdb.Int},
		},
	}
	rows := []grokdb.Tuple{
		grokdb.Tuple([]interface{}{1, 10, 5.0, 10}),
		grokdb.Tuple([]interface{}{2, 20, 4.0, 20}),
		grokdb.Tuple([]interface{}{1, 10, 5.0, 30}),
		grokdb.Tuple([]interface{}{2, 20, 5.0, 40}),

		grokdb.Tuple([]interface{}{1, 30, 3.0, 50}),
		grokdb.Tuple([]interface{}{2, 40, 3.0, 60}),
		grokdb.Tuple([]interface{}{1, 30, 5.0, 70}),
		grokdb.Tuple([]interface{}{2, 40, 4.0, 80}),

		grokdb.Tuple([]interface{}{1, 50, 2.0, 90}),
		grokdb.Tuple([]interface{}{2, 60, 2.0, 100}),
		grokdb.Tuple([]interface{}{1, 50, 4.0, 110}),
		grokdb.Tuple([]interface{}{2, 60, 3.0, 120}),
	}

	scan := InitInMemSeqScan(header, rows)
	proj := InitProjection([]grokdb.Iterator{scan}, []int{1, 2})
	group := InitGroup(
		[]grokdb.Iterator{proj},
		Column{
			Attribute: grokdb.Attribute{Type: grokdb.Int, Name: "movieId"},
			Index:     0,
		},
	)
	avg, err := InitAverage(
		[]grokdb.Iterator{group},
		Column{
			Attribute: grokdb.Attribute{Type: grokdb.Int, Name: "movieId"},
			Index:     0,
		},
		Column{
			Attribute: grokdb.Attribute{Type: grokdb.Float, Name: "rating"},
			Index:     1,
		},
	)
	if err != nil {
		// panic(err)
	}
	sort := InitSort(
		[]grokdb.Iterator{avg},
		SortBy{
			Column:     newColumn("rating", grokdb.Float, 1),
			descending: true,
		},
	)
	lim := *InitLimit([]grokdb.Iterator{sort}, 3)

	actual := make([]grokdb.Tuple, 0)
	for tup, err := lim.Next(); err != io.EOF; tup, err = lim.Next() {
		if err != nil {
			panic(err)
		}
		actual = append(actual, tup)
	}

	expected := []grokdb.Tuple{
		grokdb.Tuple([]interface{}{10, 5.0}),
		grokdb.Tuple([]interface{}{20, 4.5}),
		grokdb.Tuple([]interface{}{30, 4.0}),
	}
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("Got wrong result. Got %v. Expected %v\n", actual, expected)
	}
}

func TestSelectMoviesRatedTheMost(t *testing.T) {
	// EXPLAIN VERBOSE
	// SELECT movie_id, COUNT(rating)
	// FROM ratings
	// GROUP BY (movie_id)
	// ORDER BY COUNT(rating) DESC;

	header := grokdb.RelationHeader{
		Name: "testdata",
		Attributes: []grokdb.Attribute{
			grokdb.Attribute{Name: "userId", Type: grokdb.Int},
			grokdb.Attribute{Name: "movieId", Type: grokdb.Int},
			grokdb.Attribute{Name: "rating", Type: grokdb.Int},
			grokdb.Attribute{Name: "timestamp", Type: grokdb.Int},
		},
	}
	rows := []grokdb.Tuple{
		grokdb.Tuple([]interface{}{1, 10, 5.0, 10}),
		grokdb.Tuple([]interface{}{2, 20, 4.0, 20}),
		grokdb.Tuple([]interface{}{1, 10, 5.0, 30}),
		grokdb.Tuple([]interface{}{2, 20, 5.0, 40}),
		grokdb.Tuple([]interface{}{1, 20, 3.0, 50}),

		grokdb.Tuple([]interface{}{2, 40, 3.0, 60}),
		grokdb.Tuple([]interface{}{1, 20, 5.0, 70}),
		grokdb.Tuple([]interface{}{2, 30, 2.0, 100}),
		grokdb.Tuple([]interface{}{1, 30, 4.0, 110}),
		grokdb.Tuple([]interface{}{2, 30, 3.0, 120}),
	}

	scan := InitInMemSeqScan(header, rows)
	proj := InitProjection([]grokdb.Iterator{scan}, []int{1, 2})
	group := InitGroup(
		[]grokdb.Iterator{proj},
		Column{
			Attribute: grokdb.Attribute{Type: grokdb.Int, Name: "movieId"},
			Index:     0,
		},
	)
	count, err := InitCountByGroup(
		[]grokdb.Iterator{group},
		Column{
			Attribute: grokdb.Attribute{Type: grokdb.Int, Name: "movieId"},
			Index:     0,
		},
	)
	if err != nil {
		t.Errorf("Not expecting an error, but got one. %v\n", err)
	}
	sort := InitSort(
		[]grokdb.Iterator{count},
		SortBy{
			Column:     newColumn("movieId", grokdb.Int, 1),
			descending: true,
		},
	)

	actual := make([]grokdb.Tuple, 0)
	for tup, err := sort.Next(); err != io.EOF; tup, err = sort.Next() {
		if err != nil {
			panic(err)
		}
		actual = append(actual, tup)
	}

	expected := []grokdb.Tuple{
		grokdb.Tuple([]interface{}{20, 4}),
		grokdb.Tuple([]interface{}{30, 3}),
		grokdb.Tuple([]interface{}{10, 2}),
		grokdb.Tuple([]interface{}{40, 1}),
	}
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("Got wrong result. Got %v. Expected %v\n", actual, expected)
	}
}

package executor

import (
	"fmt"
	"io"
	"reflect"
	"testing"

	"bitbucket.org/rafaelespinoza/grokdb"
)

// PathToTestData is the relative path to test data csv files
const PathToTestData = "../testdata"

func TestInitFilescan(t *testing.T) {
	// test file not found
	_, err := InitFilescan(
		"./missing-file.csv",
		grokdb.RelationHeader{
			Name:       "mia",
			Attributes: make([]grokdb.Attribute, 0),
		},
	)
	if err == nil {
		t.Errorf("Expected an error when attempting to read a non-existent file")
	}

	// test incorrect column names
	_, err = InitFilescan(
		"./sampledata-foos.csv",
		grokdb.RelationHeader{
			Name: "foos",
			Attributes: []grokdb.Attribute{
				grokdb.Attribute{Name: "not_id", Type: grokdb.Int},
				grokdb.Attribute{Name: "not_name", Type: grokdb.String},
				grokdb.Attribute{Name: "not_is_bar", Type: grokdb.Bool},
				grokdb.Attribute{Name: "not_score", Type: grokdb.Float},
			},
		},
	)
	if err == nil {
		t.Errorf("Expected an error when headers don't match file")
	}

	// test happy path
	filescan, err := InitFilescan(
		"./sampledata-foos.csv",
		grokdb.RelationHeader{
			Name: "foos",
			Attributes: []grokdb.Attribute{
				grokdb.Attribute{Name: "id", Type: grokdb.Int},
				grokdb.Attribute{Name: "name", Type: grokdb.String},
				grokdb.Attribute{Name: "is_bar", Type: grokdb.Bool},
				grokdb.Attribute{Name: "score", Type: grokdb.Float},
			},
		},
	)
	if err != nil {
		t.Errorf("Did not expect an error")
	}

	expectedCols := []string{"id", "name", "is_bar", "score"}
	if len(filescan.header.Attributes) != len(expectedCols) {
		t.Errorf("Unexpected number of columns")
	}
	for i, attr := range filescan.header.Attributes {
		if attr.Name != expectedCols[i] {
			t.Errorf("Column does not match. Got %s, expected %s\n", attr.Name, expectedCols[i])
		}
	}
}

func TestFilescanNext(t *testing.T) {
	filescan, err := InitFilescan(
		"./sampledata-foos.csv",
		grokdb.RelationHeader{
			Name: "foos",
			Attributes: []grokdb.Attribute{
				grokdb.Attribute{Name: "id", Type: grokdb.Int},
				grokdb.Attribute{Name: "name", Type: grokdb.String},
				grokdb.Attribute{Name: "is_bar", Type: grokdb.Bool},
				grokdb.Attribute{Name: "score", Type: grokdb.Float},
			},
		},
	)
	if err != nil {
		t.Errorf("Did not expect an error, but got one: %v", err)
	}

	tuples := make([]grokdb.Tuple, 0)
	for tup, err := filescan.Next(); err != io.EOF; tup, err = filescan.Next() {
		if err != nil {
			t.Errorf("Did not expect an error, but got one: %v", err)
		}
		tuples = append(tuples, tup)
	}
	expectedTuples := []grokdb.Tuple{
		grokdb.Tuple([]interface{}{1, "Toy Story (1995)", true, 4.5}),
		grokdb.Tuple([]interface{}{2, "Jumanji (1995)", false, 4.0}),
	}

	if !reflect.DeepEqual(tuples, expectedTuples) {
		t.Errorf("Actual tuples did not equal expected tuples")
		fmt.Printf("actual:		 	%v\n", tuples)
		fmt.Printf("expected: 		%v\n", expectedTuples)
	}
}

func ExampleFilescan() {
	filescan, err := InitFilescan(
		"./sampledata-movies.csv",
		grokdb.RelationHeader{
			Name:       "movies",
			Attributes: make([]grokdb.Attribute, 0),
		},
	)
	if err != nil {
		panic(err)
	}

	for tup, err := filescan.Next(); err != io.EOF; tup, err = filescan.Next() {
		if err != nil {
			panic(err)
		}
		fmt.Printf("%+v\n", tup)
	}

	filescan.Close()
}

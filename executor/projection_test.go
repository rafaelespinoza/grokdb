package executor

import (
	"fmt"
	"io"
	"reflect"
	"testing"

	"bitbucket.org/rafaelespinoza/grokdb"
)

func TestProjectionNext(t *testing.T) {
	tuple5000 := grokdb.Tuple([]interface{}{5000, "Toy Story", true})
	tuple100 := grokdb.Tuple([]interface{}{100, "Jumanji", false})
	mockData := InitInMemSeqScan(
		grokdb.RelationHeader{
			Name: "testdata",
			Attributes: []grokdb.Attribute{
				grokdb.Attribute{Name: "id", Type: grokdb.Int},
				grokdb.Attribute{Name: "title", Type: grokdb.String},
				grokdb.Attribute{Name: "oscar", Type: grokdb.Bool},
			},
		},
		[]grokdb.Tuple{tuple5000, tuple100},
	)

	tables := []struct {
		input          []grokdb.Iterator
		colInd         []int
		expectedTuples []grokdb.Tuple
		expectedErrors []error
	}{
		{
			[]grokdb.Iterator{mockData},
			[]int{1, 2},
			[]grokdb.Tuple{
				grokdb.Tuple([]interface{}{"Toy Story", true}),
				grokdb.Tuple([]interface{}{"Jumanji", false}),
			},
			[]error{nil, nil},
		},
	}

	for i, test := range tables {
		proj := InitProjection(test.input, test.colInd)

		j := 0
		for tup, err := proj.Next(); err != io.EOF; tup, err = proj.Next() {
			if err != nil && test.expectedErrors[j] == nil {
				t.Errorf("Got an error but expected no error. Test case %d\n", i)
				return
			}
			if err == nil && test.expectedErrors[j] != nil {
				t.Errorf("Got no error but expected an error. Test case %d\n", i)
				return
			}
			if tup == nil && test.expectedTuples[j] != nil {
				t.Errorf("Got no tuple but expected a result. Test case %d\n", i)
				return
			}
			if tup != nil && test.expectedTuples[j] == nil {
				t.Errorf("Got a tuple but expected no result. Test case %d\n", i)
				return
			}
			if !reflect.DeepEqual(tup, test.expectedTuples[j]) {
				fmt.Printf("%#v\n", tup)
				fmt.Printf("%#v\n", test.expectedTuples[j])
				t.Errorf("Tuples are not deep equal. Test case %d, tuple %d\n", i, j)
				return
			}
			j++
		}

		if len(proj.tuples) != len(test.expectedTuples) {
			t.Errorf(
				"Unexpected number of results. Got %d, expected %d. Test case %d\n",
				len(proj.tuples),
				len(test.expectedErrors),
				i,
			)
		}
	}
}

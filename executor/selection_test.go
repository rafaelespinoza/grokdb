package executor

import (
	"io"
	"reflect"
	"testing"

	"bitbucket.org/rafaelespinoza/grokdb"
)

func TestSelectionNext(t *testing.T) {
	tuple5000 := grokdb.Tuple([]interface{}{5000})
	tuple100 := grokdb.Tuple([]interface{}{100})
	is5000 := func(t grokdb.Tuple) bool { return t[0] == 5000 }
	desc := grokdb.RelationHeader{
		Name: "testdata",
		Attributes: []grokdb.Attribute{
			grokdb.Attribute{Name: "id", Type: grokdb.Int},
			grokdb.Attribute{Name: "title", Type: grokdb.String},
			grokdb.Attribute{Name: "oscar", Type: grokdb.Bool},
		},
	}
	mockData0 := InitInMemSeqScan(desc, []grokdb.Tuple{tuple5000})
	mockData1 := InitInMemSeqScan(desc, []grokdb.Tuple{tuple100})
	tables := []struct {
		input          []grokdb.Iterator
		predicate      Predicate
		expectedTuples grokdb.Tuple
		expectedError  error
	}{
		{
			[]grokdb.Iterator{mockData0},
			is5000,
			tuple5000,
			nil,
		},
		{
			[]grokdb.Iterator{mockData1},
			is5000,
			nil,
			io.EOF,
		},
	}

	for i, test := range tables {
		sel := InitSelection(test.input, test.predicate)
		tup, err := sel.Next()

		if err != nil && test.expectedError == nil {
			t.Errorf("Got an error but expected no error. Test case %d\n", i)
			return
		}
		if err == nil && test.expectedError != nil {
			t.Errorf("Got no error but expected an error. Test case %d\n", i)
			return
		}
		if tup == nil && test.expectedTuples != nil {
			t.Errorf("Got no tuple but expected a result. Test case %d\n", i)
			return
		}
		if tup != nil && test.expectedTuples == nil {
			t.Errorf("Got a tuple but expected no result. Test case %d\n", i)
			return
		}
		if !reflect.DeepEqual(tup, test.expectedTuples) {
			t.Errorf("Tuples are not deep equal. Test case %d\n", i)
			return
		}
	}
}

package executor

import (
	"fmt"
	"io"
	"math"

	"bitbucket.org/rafaelespinoza/grokdb"
)

// acceptableFloatingPointEpsilon is a threshold for deciding whether or not two
// floats are "close enough" to each other to consider equal.
const acceptableFloatingPointEpsilon = 0.01

func errorNotEOF(e error) bool { return e != nil && e != io.EOF }

func mapCombineTuples(tup1, tup2 grokdb.Tuple, cols1, cols2 []int) (grokdb.Tuple, error) {
	mapped1, err := tup1.Map(cols1)
	if err != nil {
		return nil, err
	}

	mapped2, err := tup2.Map(cols2)
	if err != nil {
		return nil, err
	}

	combo := append(mapped1, mapped2...)
	return combo, nil
}

// Rounds a float64 f to the nearest value n. Assumes Go version >= 1.10.
// https://stackoverflow.com/a/39544897
func roundToNearest(f, n float64) float64 {
	return math.Round(f/n) * n
}

type Column struct {
	grokdb.Attribute
	Index int
}

func newColumn(name string, typ grokdb.Type, ind int) Column {
	return Column{
		Attribute: grokdb.Attribute{
			Name: name,
			Type: typ,
		},
		Index: ind,
	}
}

func (c Column) mustGetFloat(tup grokdb.Tuple) float64 {
	t := c.Type
	i := c.Index

	switch t {
	case grokdb.Int:
		return float64(tup[i].(int))
	case grokdb.Float:
		return tup[i].(float64)
	default:
		err := fmt.Errorf("Could not handle type %v for %+v\n", t, tup[i])
		panic(err)
	}
}

func (c Column) mustGetInt(tup grokdb.Tuple) int {
	t := c.Type
	i := c.Index

	switch t {
	case grokdb.Int:
		return tup[i].(int)
	case grokdb.Float:
		return int(tup[i].(float64))
	default:
		err := fmt.Errorf("Could not handle type %v for %+v\n", t, tup[i])
		panic(err)
	}
}

func (c Column) mustCompare(tup grokdb.Tuple, val interface{}) bool {
	t := c.Type
	i := c.Index

	switch t {
	case grokdb.Int:
		tupGroupingVal := tup[i].(int)
		currGroupingVal := val.(int)
		return tupGroupingVal == currGroupingVal
	case grokdb.Float:
		tupGroupingVal := tup[i].(float64)
		currGroupingVal := val.(float64)
		return tupGroupingVal == currGroupingVal
	default:
		err := fmt.Errorf("Could not handle type %v for %+v\n", t, tup[i])
		panic(err)
	}
}

func (c *Column) Equals(d *Column) bool {
	if c.Type != d.Type {
		return false
	}
	if c.Index != d.Index {
		return false
	}
	if c.Name != d.Name {
		return false
	}
	return true
}

package executor

import (
	"fmt"
	"io"

	"bitbucket.org/rafaelespinoza/grokdb"
)

type Group struct {
	Input   []grokdb.Iterator
	groupBy Column
	tuples  []grokdb.Tuple
}

func InitGroup(input []grokdb.Iterator, groupBy Column) *Group {
	src := input[0]
	tupleMap := make(map[Grouping][]grokdb.Tuple)

	for tup, err := src.Next(); err != io.EOF; tup, err = src.Next() {
		if err != nil {
			panic(err)
		}

		grouping := NewGrouping(groupBy, tup) // only handling single grouping for now

		if list, ok := tupleMap[grouping]; !ok {
			tupleMap[grouping] = []grokdb.Tuple{tup}
		} else {
			tupleMap[grouping] = append(list, tup)
		}
	}

	tuples := make([]grokdb.Tuple, 0)
	for _, list := range tupleMap {
		for _, tup := range list {
			tuples = append(tuples, tup)
		}
	}

	return &Group{
		Input:   input,
		groupBy: groupBy,
		tuples:  tuples,
	}
}

func (g *Group) Next() (grokdb.Tuple, error) {
	if len(g.tuples) == 0 {
		return nil, io.EOF
	}
	tup := g.tuples[0]
	g.tuples = g.tuples[1:]
	return tup, nil
}

func (g *Group) Reset() error {
	var err error

	for _, iter := range g.Input {
		err = iter.Reset()
		if err != nil {
			return err
		}
	}

	g.tuples = make([]grokdb.Tuple, 0)

	return nil
}

func (g Group) Close() error { return nil }

type Grouping struct {
	groupBy Column
	value   interface{} // Identifier for the group. Not the running aggregation.
}

func NewGrouping(groupBy Column, tup grokdb.Tuple) Grouping {
	var g Grouping
	g.groupBy = groupBy
	g.setValue(tup)
	return g
}

func (g *Grouping) setValue(tup grokdb.Tuple) {
	ind := g.groupBy.Index
	g.value = tup[ind]
}

func (g Grouping) Equals(h Grouping) bool {
	if !g.groupBy.Equals(&h.groupBy) {
		return false
	}

	groupingType := g.groupBy.Type
	switch groupingType {
	case grokdb.Bool:
		return g.value.(bool) == h.value.(bool)
	case grokdb.String:
		return g.value.(string) == h.value.(string)
	case grokdb.Int:
		return g.value.(int) == h.value.(int)
	case grokdb.Float:
		return g.value.(float64) == h.value.(float64)
	default:
		err := fmt.Errorf("Unsupported type %v", groupingType)
		panic(err)
	}
}

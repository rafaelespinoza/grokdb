package executor

import (
	"fmt"
	"io"
	"testing"

	"bitbucket.org/rafaelespinoza/grokdb"
)

func TestLimitNext(t *testing.T) {
	tables := []struct {
		filename      string
		max           int
		expCursor     int
		expNumResults int
		expEOF        bool
	}{
		{PathToTestData + "/genome-tags.csv", 100, 100, 100, true},
		{PathToTestData + "/genome-tags.csv", 1500, 1128, 1128, true},
	}

	for i, test := range tables {
		filescan, err := InitFilescan(
			test.filename,
			grokdb.RelationHeader{
				Name: "genomeTags",
				Attributes: []grokdb.Attribute{
					grokdb.Attribute{Name: "tagId", Type: grokdb.String},
					grokdb.Attribute{Name: "tag", Type: grokdb.String},
				},
			},
		)
		if err != nil {
			panic(err)
		}
		lim := *InitLimit([]grokdb.Iterator{filescan}, test.max)
		res := make([]grokdb.Tuple, 0)

		for tup, err := lim.Next(); err != io.EOF; tup, err = lim.Next() {
			if err != nil {
				panic(err)
			}
			res = append(res, tup)
		}

		if lim.cursor != test.expCursor {
			t.Errorf("Cursor not correct. Got %d, expected %d. Test case %d\n", lim.cursor, test.expCursor, i)
		}
		if len(res) != test.expNumResults {
			t.Errorf("Number of records incorrect. Got %d, expected %d. Test case %d\n", len(res), test.expNumResults, i)
		}
		if lim.eof != test.expEOF {
			t.Errorf("Value for eof incorrect. Got %t, expected %t. Test Case %d\n", lim.eof, test.expEOF, i)
		}
		fmt.Printf("length: %d\n", len(res))
	}
}

package executor

import (
	"fmt"

	"bitbucket.org/rafaelespinoza/grokdb"
)

// Project yields a subset of columns.
type Projection struct {
	Input       []grokdb.Iterator
	ColumnIndex []int
	cursor      int
	tuples      []grokdb.Tuple
}

func InitProjection(i []grokdb.Iterator, c []int) *Projection {
	return &Projection{i, c, 0, make([]grokdb.Tuple, 0)}
}

func (p *Projection) Next() (grokdb.Tuple, error) {
	input := p.Input[0]

	t, e := input.Next()
	if e != nil {
		return nil, e
	}
	p.cursor++
	if t == nil {
		return nil, nil
	}

	tup, err := t.Map(p.ColumnIndex)
	if err != nil {
		return nil, err
	}
	p.tuples = append(p.tuples, tup)
	return tup, e
}

func (p *Projection) Reset() error {
	var err error

	for _, iter := range p.Input {
		err = iter.Reset()
		if err != nil {
			return err
		}
	}

	p.cursor = 0
	p.tuples = make([]grokdb.Tuple, 0)

	return nil
}

func (p Projection) Close() error {
	fmt.Println("Projection done")
	return nil
}

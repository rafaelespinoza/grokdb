package executor

import (
	"io"

	"bitbucket.org/rafaelespinoza/grokdb"
)

// InMemSeqScan is an in-memory sequential scan that satisfies the Iterator
// interface.
type InMemSeqScan struct {
	desc   *grokdb.RelationHeader
	tuples []grokdb.Tuple
	cursor int
}

func InitInMemSeqScan(desc grokdb.RelationHeader, tuples []grokdb.Tuple) *InMemSeqScan {
	return &InMemSeqScan{
		desc:   &desc,
		tuples: tuples,
		cursor: 0,
	}
}

func (m *InMemSeqScan) Next() (grokdb.Tuple, error) {
	if m.cursor >= len(m.tuples) {
		return nil, io.EOF
	}
	tup := m.tuples[m.cursor]
	m.cursor++
	return tup, nil
}

func (m *InMemSeqScan) Reset() error {
	m.cursor = 0
	return nil
}

func (m InMemSeqScan) Close() error { return nil }

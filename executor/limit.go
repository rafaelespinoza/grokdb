package executor

import (
	"fmt"
	"io"

	"bitbucket.org/rafaelespinoza/grokdb"
)

// Limit yields available rows up to a given limit, max.
type Limit struct {
	Input  []grokdb.Iterator
	max    int
	cursor int
	eof    bool
	tuples []grokdb.Tuple
}

func InitLimit(input []grokdb.Iterator, max int) *Limit {
	return &Limit{
		Input:  input,
		max:    max,
		cursor: 0,
		eof:    false,
		tuples: make([]grokdb.Tuple, 0),
	}
}

func (p *Limit) Next() (grokdb.Tuple, error) {
	if p.cursor >= p.max {
		p.eof = true
		return nil, io.EOF
	}

	input := p.Input[0]
	t, e := input.Next()
	if e != nil {
		if e == io.EOF {
			p.eof = true
		}
		return nil, e
	}
	p.cursor++
	return t, nil
}

func (p *Limit) Reset() error {
	var err error

	for _, iter := range p.Input {
		err = iter.Reset()
		if err != nil {
			return err
		}
	}

	p.cursor = 0
	p.eof = false

	return nil
}

func (p Limit) Close() error {
	fmt.Println("Limit done")
	return nil
}

package executor

import (
	"fmt"
	"io"

	"bitbucket.org/rafaelespinoza/grokdb"
)

type Count struct {
	Input []grokdb.Iterator
	val   int
}

func InitCount(input []grokdb.Iterator) *Count {
	return &Count{
		Input: input,
		val:   0,
	}
}

func (c *Count) Next() (grokdb.Tuple, error) {
	input := c.Input[0]

	for _, err := input.Next(); err != io.EOF; _, err = input.Next() {
		if err != nil {
			return nil, err
		}
		c.val++
	}

	return grokdb.Tuple([]interface{}{c.val}), io.EOF
}

func (c Count) Close() error {
	fmt.Println("Count done")
	return nil
}

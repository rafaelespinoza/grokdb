package executor

import (
	"fmt"
	"io"

	"bitbucket.org/rafaelespinoza/grokdb"
)

// CountByGroup returns the number of tuples in the input.
type CountByGroup struct {
	Input     []grokdb.Iterator
	groupBy   Column
	nextTuple grokdb.Tuple
}

func InitCountByGroup(input []grokdb.Iterator, groupBy Column) (*CountByGroup, error) {
	if len(input) < 1 {
		return nil, fmt.Errorf("Not enough input")
	}

	tup, err := input[0].Next()
	if err != nil && err != io.EOF {
		return nil, err
	}

	count := CountByGroup{
		Input:     input,
		groupBy:   groupBy,
		nextTuple: tup, // helps determine grouping
	}

	return &count, nil
}

func (c *CountByGroup) Next() (grokdb.Tuple, error) {
	if c.nextTuple == nil {
		return nil, io.EOF
	}

	currGrouping := NewGrouping(c.groupBy, c.nextTuple)
	count := 1
	c.nextTuple = nil
	input := c.Input[0]

	for tup, err := input.Next(); err != io.EOF; tup, err = input.Next() {
		if err != nil {
			return nil, err
		}

		sameGroup := c.groupBy.mustCompare(tup, currGrouping.value)
		if !sameGroup {
			c.nextTuple = tup // establish next grouping with this tuple
			break
		}

		count += 1
	}

	return grokdb.Tuple([]interface{}{currGrouping.value, count}), nil
}

func (c *CountByGroup) Reset() error {
	var err error

	for _, iter := range c.Input {
		err = iter.Reset()
		if err != nil {
			return err
		}
	}

	c.nextTuple = nil

	return nil
}

func (c CountByGroup) Close() error {
	fmt.Println("Count done")
	return nil
}

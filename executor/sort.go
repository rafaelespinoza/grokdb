package executor

import (
	"fmt"
	"io"
	"sort"

	"bitbucket.org/rafaelespinoza/grokdb"
)

func InitSort(input []grokdb.Iterator, sortBy SortBy) *Sort {
	src := input[0]
	tuples := make([]grokdb.Tuple, 0)
	for tup, err := src.Next(); err != io.EOF; tup, err = src.Next() {
		if err != nil {
			panic(err)
		}
		tuples = append(tuples, tup)
	}

	tupleSorter := TupleSorter{
		SortBy: sortBy,
		tuples: tuples,
	}
	sort.Sort(&tupleSorter)

	return &Sort{
		Input:       input,
		TupleSorter: tupleSorter,
	}
}

// Sort yields rows in sorted order.
type Sort struct {
	Input []grokdb.Iterator
	TupleSorter
	cursor int
}

func (s *Sort) Next() (grokdb.Tuple, error) {
	if s.cursor >= len(s.tuples) {
		return nil, io.EOF
	}
	tup := s.tuples[s.cursor]
	s.cursor++
	return tup, nil
}

func (s *Sort) setCursor(j int) error {
	s.cursor = j
	if s.cursor >= len(s.tuples) {
		return io.EOF
	}
	return nil
}

func (s *Sort) Reset() error {
	var err error

	for _, iter := range s.Input {
		err = iter.Reset()
		if err != nil {
			return err
		}
	}

	s.tuples = make([]grokdb.Tuple, 0)

	return nil
}

func (s Sort) Close() error {
	return nil
}

// TupleSorter is a descriptor for sorting a collection of Tuples. It
// implements sort.Interface.
type TupleSorter struct {
	SortBy
	tuples []grokdb.Tuple
}

func (s *TupleSorter) Len() int {
	return len(s.tuples)
}

func (s *TupleSorter) Less(i, j int) bool {
	colInd := s.Index
	left := s.tuples[i][colInd]
	right := s.tuples[j][colInd]

	if s.descending {
		return cmp(s.Type, right, left)
	}
	return cmp(s.Type, left, right)
}

func (s *TupleSorter) Swap(i, j int) {
	s.tuples[i], s.tuples[j] = s.tuples[j], s.tuples[i]
}

type SortBy struct {
	Column
	descending bool
}

func cmp(valType grokdb.Type, left, right interface{}) bool {
	switch valType {
	case grokdb.Int:
		return left.(int) < right.(int)
	case grokdb.Float:
		return left.(float64) < right.(float64)
	case grokdb.String:
		lt := left.(string)
		rt := right.(string)

		if rt > lt {
			return false
		}
		return true
	default:
		err := fmt.Errorf("Unsupported type %v", valType)
		panic(err)
	}
}

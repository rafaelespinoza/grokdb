package executor

import (
	"fmt"
	"io"

	"bitbucket.org/rafaelespinoza/grokdb"
)

type Theta func(a, b grokdb.Tuple) bool

type joinRelation struct {
	grokdb.Iterator
	colInd []int  // indices of columns to select from when mapping results
	joinOn Column // describes the join column
}

func InitNestedLoopJoin(inputs []joinRelation, theta Theta) (*NestedLoopJoin, error) {
	join := NestedLoopJoin{
		Inputs: inputs,
		Theta:  theta,
	}
	return &join, nil
}

type NestedLoopJoin struct {
	Inputs []joinRelation
	Theta
}

func (j *NestedLoopJoin) Next() (grokdb.Tuple, error) {
	outerRel := j.Inputs[0]
	innerRel := j.Inputs[1]

	for outerTup, oerr := outerRel.Next(); oerr != io.EOF; outerTup, oerr = outerRel.Next() {
		if oerr != nil {
			return nil, oerr
		}

		for {
			innerTup, ierr := innerRel.Next()
			if ierr == io.EOF {
				resetErr := innerRel.Reset()
				if resetErr != nil {
					return nil, resetErr
				}
				break
			} else if ierr != nil {
				return nil, ierr
			}

			if j.Theta(outerTup, innerTup) {
				resetErr := outerRel.Reset()
				if resetErr != nil {
					return nil, resetErr
				}

				return mapCombineTuples(
					outerTup,
					innerTup,
					outerRel.colInd,
					innerRel.colInd,
				)
			}
		}
	}

	return nil, io.EOF
}

func (j *NestedLoopJoin) Reset() error {
	var err error

	for _, iter := range j.Inputs {
		err = iter.Reset()
		if err != nil {
			return err
		}
	}

	return nil
}

func (j NestedLoopJoin) Close() error { return nil }

type SortMergeJoin struct {
	Input       []joinRelation
	same        Theta
	lessThan    Theta
	results     []grokdb.Tuple
	cursor      int
	outerSorter *Sort
	innerSorter *Sort
}

func InitSortMergeJoin(
	inputs []joinRelation,
	sortDescA SortBy,
	sortDescB SortBy,
	same Theta,
	lessThan Theta,
) (*SortMergeJoin, error) {
	if len(inputs) != 2 {
		return nil, fmt.Errorf("Need two inputs. Got %d", len(inputs))
	}

	sortA := InitSort([]grokdb.Iterator{inputs[0].Iterator}, sortDescA)
	sortB := InitSort([]grokdb.Iterator{inputs[1].Iterator}, sortDescB)

	smj := SortMergeJoin{
		Input: []joinRelation{
			joinRelation{
				Iterator: sortA,
				colInd:   inputs[0].colInd,
				joinOn:   inputs[0].joinOn, // TODO: necessary? already have theta
			},
			joinRelation{
				Iterator: sortB,
				colInd:   inputs[1].colInd,
				joinOn:   inputs[1].joinOn, // TODO: necessary? already have theta
			},
		},
		same:        same,
		lessThan:    lessThan,
		outerSorter: sortA,
		innerSorter: sortB,
	}

	err := smj.merge()
	if err != nil {
		return nil, err
	}

	return &smj, nil
}

func (s *SortMergeJoin) Next() (grokdb.Tuple, error) {
	if s.cursor >= len(s.results) {
		return nil, io.EOF
	}
	tup := s.results[s.cursor]
	s.cursor++
	return tup, nil
}

// http://web.cs.ucla.edu/classes/fall14/cs143/notes/join.pdf
func (s *SortMergeJoin) merge() error {
	var err error

	outerTup, err := s.outerSorter.Next()
	if err != nil {
		return err
	}

	innerTup, err := s.innerSorter.Next()
	if err != nil {
		return err
	}

	for outerTup != nil && innerTup != nil {
		if s.same(outerTup, innerTup) {
			j := s.innerSorter.cursor
			err := s.output(&outerTup, &innerTup)
			if errorNotEOF(err) {
				return err
			}
			s.innerSorter.setCursor(j) // "rewind"
		} else if s.lessThan(outerTup, innerTup) {
			outerTup, err = s.outerSorter.Next()
			if errorNotEOF(err) {
				return err
			}
		} else {
			innerTup, err = s.innerSorter.Next()
			if errorNotEOF(err) {
				return err
			}
		}
	}

	return nil
}

func (s *SortMergeJoin) output(outerTup, innerTup *grokdb.Tuple) error {
	var err error
	outerCols := s.Input[0].colInd
	innerCols := s.Input[1].colInd

	for *outerTup != nil && s.same(*outerTup, *innerTup) {
		for *innerTup != nil && s.same(*outerTup, *innerTup) {
			match, err := mapCombineTuples(
				*outerTup,
				*innerTup,
				outerCols,
				innerCols,
			)
			if err != nil {
				return err
			}

			s.results = append(s.results, match)

			*innerTup, err = s.innerSorter.Next()
			if errorNotEOF(err) {
				return err
			}
		}

		*outerTup, err = s.outerSorter.Next()
		if errorNotEOF(err) {
			return err
		}
	}

	return nil
}

func (s SortMergeJoin) Close() error { return nil }

type ClassicHashJoin struct {
	Input   []joinRelation
	Outer   hashPartitions
	Inner   hashPartitions
	results []grokdb.Tuple
	cursor  int
}

func InitClassicHashJoin(input []joinRelation) (*ClassicHashJoin, error) {
	var join ClassicHashJoin
	join.Input = input

	outer, err := join.partitionTable(input[0].Iterator, input[0].joinOn)
	if err != nil {
		return nil, err
	}
	inner, err := join.partitionTable(input[1].Iterator, input[1].joinOn)
	if err != nil {
		return nil, err
	}

	join.Outer = outer
	join.Inner = inner
	results, err := join.buildAndProbe()
	if err != nil {
		return nil, err
	}
	join.results = results
	join.cursor = 0
	return &join, nil
}

func (j *ClassicHashJoin) Next() (grokdb.Tuple, error) {
	if j.cursor >= len(j.results) {
		return nil, io.EOF
	}
	tup := j.results[j.cursor]
	j.cursor++
	return tup, nil
}

func (j ClassicHashJoin) Close() error {
	return nil
}

func (j *ClassicHashJoin) Reset() error {
	var err error

	for _, iter := range j.Input {
		err = iter.Reset()
		if err != nil {
			return err
		}
	}

	j.Outer = newHashPartitions()
	j.Inner = newHashPartitions()
	j.cursor = 0

	return nil
}

func (j ClassicHashJoin) partitionTable(input grokdb.Iterator, joinOn Column) (hashPartitions, error) {
	parts, err := initHashPartitions(
		input,
		joinOn.Type,
		joinOn.Index,
		10,
	)
	if err != nil {
		return nil, err
	}

	return *parts, nil
}

func (j *ClassicHashJoin) buildAndProbe() ([]grokdb.Tuple, error) {
	results := make([]grokdb.Tuple, 0)
	outerIndex := j.Input[0].joinOn.Index
	innerIndex := j.Input[1].joinOn.Index

	for outerKey := range j.Outer {
		hashTable := j.hashOnePartition(outerKey, outerIndex)

		var innerTups []grokdb.Tuple
		if tups, ok := j.Inner[outerKey]; ok {
			innerTups = tups
		} else {
			fmt.Printf("Could not find key %q in inner partition", outerKey)
			continue
		}

		matches, err := j.probeResults(
			hashTable,
			innerTups,
			innerIndex,
		)
		if err != nil {
			return nil, err
		}
		results = append(results, matches...)
	}

	return results, nil
}

func (j *ClassicHashJoin) probeResults(
	hashTable HashTable,
	innerTups []grokdb.Tuple,
	tupleInd int,
) ([]grokdb.Tuple, error) {
	matches := make([]grokdb.Tuple, 0)

	for _, innerTup := range innerTups {
		key := innerTup[tupleInd]
		var outerTups []grokdb.Tuple
		if tups, ok := hashTable[key]; !ok {
			continue
		} else {
			outerTups = tups
		}

		for _, outerTup := range outerTups {
			match, err := mapCombineTuples(
				outerTup,
				innerTup,
				j.Input[0].colInd,
				j.Input[1].colInd,
			)
			if err != nil {
				return nil, err
			}
			matches = append(matches, match)
		}
	}

	return matches, nil
}

// hashOnePartition makes a hash table out of one partition in the outer table,
// otherwise known as the "build" input.
func (j ClassicHashJoin) hashOnePartition(key string, ind int) HashTable {
	table := newHashTable()
	tuples := j.Outer[key]

	for _, tup := range tuples {
		joinKey := tup[ind]
		if tups, ok := table[joinKey]; !ok {
			table[joinKey] = []grokdb.Tuple{tup}
		} else {
			table[joinKey] = append(tups, tup)
		}
	}

	return table
}

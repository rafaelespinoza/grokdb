# grokdb

A simple relational database management system.
Course project for Bradfield databases class.

#### test data

The unit tests are expecting the
[MovieLens](https://grouplens.org/datasets/movielens/20m/) data set to be in
`testdata/`. It's almost 1 GB of CSV files and it's gitignored. If you don't
want to duplicate the files on your local, just symlink them:

```sh
ln -sv $PATH_TO_CSV_FILES ./testdata`
```

package storage

import (
	"bytes"
	"testing"

	"bitbucket.org/rafaelespinoza/grokdb"
)

func TestParseHeader(t *testing.T) {
	inputHeader := grokdb.RelationHeader{
		Name: "players",
		Attributes: []grokdb.Attribute{
			{Name: "id", Type: grokdb.Int},
			{Name: "name", Type: grokdb.String},
			{Name: "active", Type: grokdb.Bool},
			{Name: "score", Type: grokdb.Float},
		},
	}
	initCatalogs(&inputHeader)

	formattedHeader := []byte{
		0x00, 0x01, // ID
		0x07,                                     // length_of_name <uint8>
		0x04,                                     // num_attributes <uint8>
		0x70, 0x6C, 0x61, 0x79, 0x65, 0x72, 0x73, // Name <string>
		0x00, 0x01, 0x00, 0x02, 0x00, 0x03, 0x00, 0x04, // []attribute_id
	}

	expected := grokdb.RelationHeader{
		Name: "players",
		ID:   1,
		Attributes: []grokdb.Attribute{
			{ID: 1, Name: "id", Type: grokdb.Int},
			{ID: 2, Name: "name", Type: grokdb.String},
			{ID: 3, Name: "active", Type: grokdb.Bool},
			{ID: 4, Name: "score", Type: grokdb.Float},
		},
	}

	reader := bytes.NewReader(formattedHeader)
	result, err := parseHeader(reader)
	if err != nil {
		t.Error(err)
		return
	}
	if result.ID != expected.ID {
		t.Errorf("wrong id, got %d, expected %d", result.ID, expected.ID)
	}
	if result.Name != expected.Name {
		t.Errorf(
			"wrong Name; got %q, expected %q",
			result.Name, expected.Name,
		)
	}
	if len(result.Attributes) != len(expected.Attributes) {
		t.Errorf(
			"wrong length of Attributes; got %d expected %d",
			len(result.Attributes), len(expected.Attributes),
		)
	}
	for i, attr := range result.Attributes {
		if attr.ID != expected.Attributes[i].ID {
			t.Errorf(
				"Test %d, wrong ID; got %d, expected %d",
				i, attr.ID, expected.Attributes[i].ID,
			)
		}
		if attr.Name != expected.Attributes[i].Name {
			t.Errorf(
				"Test %d, wrong Name; got %s, expected %s",
				i, attr.Name, expected.Attributes[i].Name,
			)
		}
		if attr.Type != expected.Attributes[i].Type {
			t.Errorf(
				"Test %d, wrong Type; got %s, expected %s",
				i, attr.Type, expected.Attributes[i].Type,
			)
		}
	}
}

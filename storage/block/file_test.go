package block_test

import (
	"bytes"
	"os"
	"strings"
	"testing"

	"bitbucket.org/rafaelespinoza/grokdb/storage/block"
)

func TestFile(t *testing.T) {
	const testFileDir = "tmp_test_files"
	os.Mkdir(testFileDir, 0744)
	defer func() {
		os.RemoveAll(testFileDir)
	}()

	const blockSize = 1 << 14
	contents := []string{ // length of each entry should be <= blockSize
		"alpha", "bravo", "charlie", "delta", "echo", "foxtrot", "golf", "hotel", "india", "juliett",
	}
	numBlocks := int32(len(contents))

	t.Run("all", func(t *testing.T) {
		var foo *block.File
		defer func() { foo.Close() }()

		foo, err := block.OpenFile(testFileDir+"/all", blockSize)
		if err != nil {
			t.Error(err)
			return
		}
		if blockID, err := foo.Allocate(numBlocks); err != nil {
			t.Error(err)
			return
		} else if blockID != int32(0) {
			t.Errorf("expected blockID to be %d, got %d", int32(0), blockID)
		}
		if foo.NumBlocks != numBlocks {
			t.Errorf("expected numBlocks to be %d, got %d", numBlocks, foo.NumBlocks)
		}

		for i, word := range contents {
			var err error
			bID := int32(i)

			n := blockSize - len(word)
			in := make([]byte, n)
			in = append(in, []byte(word)...)
			if err = foo.WriteAt(in, bID); err != nil {
				t.Error(err)
			}

			out := make([]byte, blockSize)
			if err := foo.ReadAt(out, int32(i)); err != nil {
				t.Error(err)
			}
			if !bytes.Equal(out, in) {
				t.Errorf("wrong read contents. Got %q, expected %q", out, in)
			}
		}
	})

	t.Run("OpenFile", func(t *testing.T) {
		if _, err := block.OpenFile(testFileDir+"/OpenFile", -1); err == nil {
			t.Errorf("expected error, got nil")
		}
	})

	t.Run("Allocate", func(t *testing.T) {
		bar, err := block.OpenFile(testFileDir+"/Allocate", blockSize)
		if err != nil {
			t.Error(err)
		}
		blockID, err := bar.Allocate(-1)
		if err == nil {
			t.Errorf("expected error, got nil")
		}
		if blockID != int32(0) {
			t.Errorf("expected blockID to be %d, got %d", int32(0), blockID)
		}
	})

	t.Run("WriteAt", func(t *testing.T) {
		bar, err := block.OpenFile(testFileDir+"/WriteAt", blockSize)
		if err != nil {
			t.Error(err)
			return
		}
		if _, err = bar.Allocate(1); err != nil {
			t.Error(err)
			return
		}

		var in []byte
		var bID int32

		in = []byte(strings.Repeat("*", bar.BlockSize+1))
		if err := bar.WriteAt(in, bID); err == nil {
			t.Error("expected error for invalid input to WriteAt, length of b")
		}

		in = []byte(strings.Repeat("*", bar.BlockSize-1))
		if err := bar.WriteAt(in, bID); err == nil {
			t.Error("expected error for invalid input to WriteAt, length of b")
		}

		in = []byte(strings.Repeat("*", bar.BlockSize))
		bID = int32(-1)
		if err := bar.WriteAt(in, bID); err == nil {
			t.Error("expected error for invalid input to WriteAt, blockID too low")
		}

		in = []byte(strings.Repeat("*", bar.BlockSize))
		bID = int32(1) + bar.NumBlocks
		if err := bar.WriteAt(in, bID); err == nil {
			t.Error("expected error for invalid input to WriteAt, blockID too high")
		}
	})

	t.Run("ReadAt", func(t *testing.T) {
		bar, err := block.OpenFile(testFileDir+"/ReadAt", blockSize)
		if err != nil {
			t.Error(err)
			return
		}
		if _, err = bar.Allocate(1); err != nil {
			t.Error(err)
			return
		}

		var in, out []byte
		var bID int32

		in = []byte(strings.Repeat("*", bar.BlockSize))
		if err := bar.WriteAt(in, bID); err != nil {
			t.Error(err)
			return
		}

		out = make([]byte, blockSize+1)
		if err := bar.ReadAt(out, bID); err == nil {
			t.Error("expected error for invalid input to ReadAt, length of b")
		}

		out = make([]byte, blockSize)
		bID = int32(-1)
		if err := bar.ReadAt(out, bID); err == nil {
			t.Error("expected error for invalid input to ReadAt, blockID too low")
		}

		out = make([]byte, blockSize)
		bID = int32(1) + bar.NumBlocks
		if err := bar.ReadAt(out, bID); err == nil {
			t.Error("expected error for invalid input to ReadAt, blockID too high")
		}
	})
}

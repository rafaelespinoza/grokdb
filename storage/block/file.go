package block

import (
	"fmt"
	"os"
)

// InvalidBlockID signifies an error in allocating a File.
const InvalidBlockID = -1

// File is a wrapper around os.File.
type File struct {
	BlockSize int
	NumBlocks int32

	osFile *os.File
}

// OpenFile initializes a File at path, where blockSize is the number of bytes
// in one block. It will create a file if it doesn't exist or open it if it
// already does.
func OpenFile(path string, blockSize int) (out *File, err error) {
	if blockSize < 0 {
		err = fmt.Errorf("blockSize must be non-negative")
		return
	}
	var file *os.File
	var stat os.FileInfo

	if file, err = os.OpenFile(path, os.O_RDWR|os.O_CREATE, 0644); err != nil {
		return
	}
	if stat, err = file.Stat(); err != nil {
		return
	}
	out = &File{
		osFile:    file,
		BlockSize: blockSize,
		NumBlocks: int32(stat.Size() / int64(blockSize)),
	}
	return
}

// Close makes the file unusable for I/O. Use OpenFile to re-enable I/O.
func (f *File) Close() error {
	return f.osFile.Close()
}

// Allocate increases the size of f's internal file by num blocks. It returns
// the blockID of the newly allocated block and guarantees that the next blockID
// will be the current value of f.NumBlocks.
func (f *File) Allocate(num int32) (blockID int32, err error) {
	blockID = f.NumBlocks
	if num < 0 {
		err = fmt.Errorf("num must be non-negative")
		return
	}
	f.NumBlocks += num
	err = f.osFile.Truncate(int64(f.NumBlocks) * int64(f.BlockSize))
	if err != nil {
		return InvalidBlockID, err
	}
	return
}

// ReadAt reads the contents of the file at blockID into b. It returns an error
// when blockID is too low or too high. It also returns an error if the length
// of b does not match the BlockSize of f.
func (f *File) ReadAt(b []byte, blockID int32) (err error) {
	if err = f.checkBounds(b, blockID); err != nil {
		return
	}
	_, err = f.osFile.ReadAt(b, int64(blockID)*int64(f.BlockSize))
	return
}

// WriteAt writes the contents of b to the file at blockID. It returns an error
// when blockID is too low or too high. It also returns an error if the length
// of b does not match the BlockSize of f.
func (f *File) WriteAt(b []byte, blockID int32) (err error) {
	if err = f.checkBounds(b, blockID); err != nil {
		return
	}
	_, err = f.osFile.WriteAt(b, int64(blockID)*int64(f.BlockSize))
	return
}

func (f *File) checkBounds(b []byte, blockID int32) error {
	if blockID < 0 || blockID >= f.NumBlocks {
		return fmt.Errorf("blockID must be in [0, %d); got %d", f.NumBlocks, blockID)
	}
	if len(b) != f.BlockSize {
		return fmt.Errorf("len(b) must be %d; got %d", f.BlockSize, len(b))
	}
	return nil
}

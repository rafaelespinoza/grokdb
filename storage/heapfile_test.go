package storage

import (
	"os"
	"testing"
	"time"

	"bitbucket.org/rafaelespinoza/grokdb"
	"bitbucket.org/rafaelespinoza/grokdb/executor"
)

const (
	testFileDir = "tmp_test_files"

	// PathToTestData is the relative path to test data csv files
	PathToTestData = "../testdata"
)

func TestHeapFile(t *testing.T) {
	defer os.RemoveAll(testFileDir)

	header := grokdb.RelationHeader{
		Name: "movies",
		Attributes: []grokdb.Attribute{
			grokdb.Attribute{Name: "movieId", Type: grokdb.Int},
			grokdb.Attribute{Name: "title", Type: grokdb.String},
			grokdb.Attribute{Name: "year", Type: grokdb.Int},
		},
	}

	os.Mkdir(testFileDir, 0744)
	initCatalogs(&header)
	const filepath = testFileDir + "/TestHeapFile"

	hf, err := NewHeapFile(filepath, &header)
	if err != nil {
		t.Error(err)
		return
	}
	tuples := []grokdb.Tuple{
		([]interface{}{1, "Toy Story", 1995}),
		([]interface{}{2, "Jumanji", 1996}),
		([]interface{}{3, "Men In Black", 1997}),
	}
	for i, tup := range tuples {
		id, err := hf.Insert(tup)
		if err != nil {
			t.Errorf("tuple %d, got error %v", i, err)
		}
		if id.Page != uint16(0) {
			t.Errorf(
				"expected %d for page ID; got %d",
				uint16(0), id.Page,
			)
		}
		if id.Slot != uint16(i) {
			t.Errorf(
				"expected %d for slot ID; got %d",
				uint16(i), id.Slot,
			)
		}
	}
	if err := hf.Close(); err != nil {
		t.Error(err)
		return
	}

	// test re-opening the file and parsing the Page
	reOpenedHF, err := OpenHeapFile(filepath, &header)
	if err != nil {
		t.Error(err)
		return
	}
	page := reOpenedHF.page
	if page.relationHeader.Name != header.Name {
		t.Errorf(
			"expected %s for Name; got %s",
			header.Name, page.relationHeader.Name,
		)
	}
	for i, exp := range header.Attributes {
		actual := page.relationHeader.Attributes[i]
		if actual.Name != exp.Name {
			t.Errorf(
				"expected %s for attribute[%d].Name; got %s",
				exp.Name, i, actual.Name,
			)
		}
		if actual.Type != exp.Type {
			t.Errorf(
				"expected %s for attribute[%d].Type; got %s",
				exp.Type, i, actual.Type,
			)
		}
	}

	// test Page.Header
	const tupleHeaderLength = 4 + (4 * 3) + 2
	expectedPageHeader := struct{ lower, upper uint16 }{
		lower: 6 + (4 * 3),
		upper: pageSize - (25 + tupleHeaderLength) - (23 + tupleHeaderLength) - (28 + tupleHeaderLength),
	}
	if page.Header.Lower != expectedPageHeader.lower {
		t.Errorf(
			"expected %d for page.Header.Lower; got %d",
			expectedPageHeader.lower, page.Header.Lower,
		)
	}
	if page.Header.Upper != expectedPageHeader.upper {
		t.Errorf(
			"expected %d for page.Header.Upper; got %d",
			expectedPageHeader.upper, page.Header.Upper,
		)
	}

	// test Page.LinePointers
	expectedPageLinePointers := []struct{ offset, length uint16 }{
		{pageSize - (25 + tupleHeaderLength*1), 25 + tupleHeaderLength},
		{pageSize - (48 + tupleHeaderLength*2), 23 + tupleHeaderLength},
		{pageSize - (76 + tupleHeaderLength*3), 28 + tupleHeaderLength},
	}
	if len(page.LinePointers) != len(expectedPageLinePointers) {
		t.Errorf(
			"expected %d line pointers; got %d",
			len(expectedPageLinePointers), len(page.LinePointers),
		)
	}
	for i, exp := range expectedPageLinePointers {
		if page.LinePointers[i].Offset != exp.offset {
			t.Errorf(
				"expected %d for page.LinePointers[%d].Offset; got %d",
				exp.offset, i, page.LinePointers[i].Offset,
			)
		}
		if page.LinePointers[i].Length != exp.length {
			t.Errorf(
				"expected %d for page.LinePointers[%d].Length; got %d",
				exp.length, i, page.LinePointers[i].Length,
			)
		}
	}

	// test Page.Items
	if len(page.Items) != len(tuples) {
		t.Errorf(
			"expected %d items; got %d",
			len(tuples), len(page.Items),
		)
	}
	expectedItemHeaders := []struct {
		IDs        *TupleID
		Attributes []*TupleAttribute
		size       uint16 // length of entire tuple in bytes
	}{
		{
			IDs: &TupleID{Page: page.id, Slot: 0},
			Attributes: []*TupleAttribute{
				{ID: 1, Offset: 0},
				{ID: 2, Offset: 8},
				{ID: 3, Offset: 17},
			},
			size: 43,
		},
		{
			IDs: &TupleID{Page: page.id, Slot: 1},
			Attributes: []*TupleAttribute{
				{ID: 1, Offset: 0},
				{ID: 2, Offset: 8},
				{ID: 3, Offset: 15},
			},
			size: 41,
		},
		{
			IDs: &TupleID{Page: page.id, Slot: 2},
			Attributes: []*TupleAttribute{
				{ID: 1, Offset: 0},
				{ID: 2, Offset: 8},
				{ID: 3, Offset: 20},
			},
			size: 46,
		},
	}
	for i, exp := range expectedItemHeaders {
		act := page.Items[i].Header
		if act.IDs.Page != exp.IDs.Page {
			t.Errorf(
				"expected %d for [%d]Header.IDs.Page; got %d",
				exp.IDs.Page, i, act.IDs.Page,
			)
		}
		if act.IDs.Slot != exp.IDs.Slot {
			t.Errorf(
				"expected %d for [%d]Header.IDs.Slot; got %d",
				exp.IDs.Slot, i, act.IDs.Slot,
			)
		}
		for j, expAttr := range exp.Attributes {
			actAttr := act.Attributes[j]
			if actAttr.ID != expAttr.ID {
				t.Errorf(
					"expected %d for [%d]Header.Attibutes[%d].ID; got %d",
					exp.IDs.Slot, i, j, actAttr.ID,
				)
			}
			if actAttr.Offset != expAttr.Offset {
				t.Errorf(
					"expected %d for [%d]Header.Attibutes[%d].Offset; got %d",
					exp.IDs.Slot, i, j, actAttr.Offset,
				)
			}
		}
		if act.size != exp.size {
			t.Errorf(
				"expected %d for [%d]Header.size; got %d",
				exp.size, i, act.size,
			)
		}
	}
	expectedItemDataLengths := []int{25, 23, 28}
	if len(page.Items) != len(expectedItemDataLengths) {
		t.Errorf(
			"expected %d items; got %d",
			len(expectedItemDataLengths), len(page.Items),
		)
	}
	for i, exp := range expectedItemDataLengths {
		if len(page.Items[i].Data) != exp {
			t.Errorf(
				"expected %d for len(page.Items[%d].Data); got %d",
				exp, i, len(page.Items[i].Data),
			)
		}
	}
	for i, exp := range tuples {
		act, err := page.Items[i].ToTuple()
		if err != nil {
			t.Errorf("not expecting error %v at index %d", err, i)
			continue
		}
		if len(act) != len(exp) {
			t.Errorf(
				"expected length %d for Tuple at page.Items[%d]; got %d",
				len(exp), i, len(act),
			)
		}
	}
}

func TestHeapFileBulkLoad(t *testing.T) {
	defer os.RemoveAll(testFileDir)

	header := grokdb.RelationHeader{
		Name: "movies",
		Attributes: []grokdb.Attribute{
			grokdb.Attribute{Name: "movieId", Type: grokdb.Int},
			grokdb.Attribute{Name: "title", Type: grokdb.String},
			grokdb.Attribute{Name: "year", Type: grokdb.Int},
		},
	}

	os.Mkdir(testFileDir, 0744)
	initCatalogs(&header)

	const (
		heapFilename = testFileDir + "/TestHeapFileBulkLoad"
		csvFilename  = "../executor/sampledata-movies.csv"
	)

	hf, err := NewHeapFile(heapFilename, &header)
	if err != nil {
		t.Error(err)
		return
	}

	// load CSV
	t.Log("Starting timer...")
	start := time.Now()
	csvScan, err := executor.InitFilescan(csvFilename, header)
	if err != nil {
		t.Error(err)
		return
	}
	if err = hf.BulkLoad(csvScan); err != nil {
		t.Error(err)
		return
	}
	t.Logf(
		"Done bulk loading heap file %v after %v\n",
		heapFilename,
		time.Since(start),
	)
}

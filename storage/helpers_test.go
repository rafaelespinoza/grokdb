package storage

import (
	"bitbucket.org/rafaelespinoza/grokdb"
	"bitbucket.org/rafaelespinoza/grokdb/catalog"
)

// test helpers go here

// initCatalogs empties the system catalogs and re-initializes them to the
// contents of headers.
func initCatalogs(headers ...*grokdb.RelationHeader) error {
	catalog.Reboot()

	for i, header := range headers {
		relID, err := catalog.CreateTable(header.Name, header.Attributes...)
		if err != nil {
			return err
		}
		headers[i].ID = relID

		for j, attr := range header.Attributes {
			if attr.ID != 0 {
				continue // assume it's already registered correctly
			}
			id, err := catalog.Attributes.Put(attr)
			if err != nil {
				return err
			}
			headers[i].Attributes[j].ID = id
		}
	}

	return nil
}

package storage

import (
	"bytes"
	"encoding/binary"
	"fmt"

	"bitbucket.org/rafaelespinoza/grokdb"
	"bitbucket.org/rafaelespinoza/grokdb/catalog"
)

// ItemPointerState describes possible states of an item pointer
type ItemPointerState uint8

const (
	// Unused means available for immediate re-use
	Unused ItemPointerState = iota
	// Normal means it is used.
	Normal
	// Redirect is reserved for later use.
	Redirect
	// Dead means this probably does not store anything.
	Dead
)

func (p ItemPointerState) String() string {
	states := [...]string{
		"Unused",
		"Normal",
		"Redirect",
		"Dead",
	}

	if p < Unused || p > Dead {
		return "Dead"
	}

	return states[p]
}

// attributeWidthsByType associates a type T with the number of bytes taken up
// by a value of type T. Variable length data is indicated by -1.
var attributeWidthsByType = map[grokdb.Type]int8{
	grokdb.Int:    8,
	grokdb.Float:  8,
	grokdb.Bool:   1,
	grokdb.String: -1,
}

// The following constants are the widths, in bytes, of metadata values.
const (
	pageHeaderWidth     = 6
	itemIDWidth         = 4
	tupleIDWidth        = 4
	tupleAttributeWidth = 4
	tupleSize           = 2
)

// HeapRelationHeader is a thin wrapper around grokdb.RelationHeader. The ID
// field is its key in the catalog.
type HeapRelationHeader struct {
	grokdb.RelationHeader
}

// Format outputs the ID, the name, number of attributes and the name and type
// of each attribute in h.
func (h HeapRelationHeader) Format() ([]byte, error) {
	w := new(bytes.Buffer)
	var err error
	if err = binary.Write(w, ByteOrder, h.ID); err != nil {
		return nil, err
	}
	name := h.Name
	attributes := h.Attributes
	if err = binary.Write(w, ByteOrder, uint8(len(name))); err != nil {
		return nil, err
	}
	if err = binary.Write(w, ByteOrder, uint8(len(attributes))); err != nil {
		return nil, err
	}
	if err = binary.Write(w, ByteOrder, []byte(name)); err != nil {
		return nil, err
	}
	for _, attr := range attributes {
		if err = binary.Write(w, ByteOrder, attr.ID); err != nil {
			return nil, err
		}
	}
	return w.Bytes(), nil
}

// A Page is an abstraction over a unit of I/O (a disk block).
type Page struct {
	// Header is modeled after PageHeaderData in postgres.
	Header struct {
		RelationID uint16 // Key of relation in catalog
		Lower      uint16 // Offset to start of free space (last line pointer)
		Upper      uint16 // Offset to end of free space (last tuple)
	}

	// List of offset, length pairs pointing to the actual items. It's also
	// known as a slotted page directory.
	LinePointers []*ItemIDData

	// The actual items.
	Items []*HeapTuple

	relationHeader  *grokdb.RelationHeader
	tupleAttributes []TupleAttribute
	id              uint16
	slots           uint16
}

// NewPage initializes the contents of a Page.
func NewPage(header *grokdb.RelationHeader, blockID, size uint16) (*Page, error) {
	tupAttrs := make([]TupleAttribute, len(header.Attributes))
	for i, attr := range header.Attributes {
		tupAttrs[i] = TupleAttribute{ID: attr.ID}
	}

	return &Page{
		Header: struct{ RelationID, Lower, Upper uint16 }{
			RelationID: header.ID,
			Lower:      uint16(pageHeaderWidth),
			Upper:      size,
		},
		LinePointers:    make([]*ItemIDData, 0),
		Items:           make([]*HeapTuple, 0),
		relationHeader:  header,
		tupleAttributes: tupAttrs,
		id:              blockID,
		slots:           0,
	}, nil
}

// Put appends tup to the Items field of p and returns the slot number id of the
// newly-added tuple. It returns an error for invalid tuple data or returns an
// EOP error when the Page is out of room. In the case of an EOP error, the
// caller should add the tuple to a new Page.
func (p *Page) Put(tup grokdb.Tuple) (id TupleID, err error) {
	// NOTE: you might want to use a lock
	id = TupleID{Page: p.id, Slot: p.slots}
	htup, err := NewHeapTuple(
		p.relationHeader,
		id,
		&tup,
	)
	if err != nil {
		return
	}

	nextLower := p.Header.Lower + uint16(itemIDWidth)
	nextUpper := p.Header.Upper - htup.Header.size
	if nextLower >= nextUpper { // not enough room to store new HeapTuple?
		id = TupleID{}
		err = EOP{
			pageID:             p.id,
			linePointersOffset: p.Header.Lower,
			itemsOffset:        p.Header.Upper,
			tupleLength:        htup.Header.size,
		}
		return
	}

	p.Header.Lower = nextLower
	p.Header.Upper = nextUpper
	p.LinePointers = append(
		p.LinePointers,
		&ItemIDData{
			Offset: p.Header.Upper,
			Length: htup.Header.size,
		},
	)
	p.Items = append(p.Items, htup)
	p.slots++
	return
}

// Get returns the HeapTuple at an index slot from the Items field of p. It
// returns an error if slot is out of bounds or Items[slot] is empty.
func (p *Page) Get(slot uint16) (out HeapTuple, err error) {
	if slot > p.slots {
		err = fmt.Errorf("slot %d is out of bounds", slot)
		return
	}
	item := p.Items[slot]
	if item == nil {
		err = fmt.Errorf("slot %d is empty", slot)
		return
	}
	out = *item
	return
}

// Format returns p in binary form. Items are serialized in reverse order: the
// first tuple is at the very end of the page and subsequent tuples progress
// towards the beginning of the page. Free space is inserted between the line
// pointers and tuples until the length of the output matches the page size.
func (p Page) Format() ([]byte, error) {
	var out []byte
	var err error
	buf := new(bytes.Buffer)

	if err = writeUint16(buf, p.Header.RelationID, p.Header.Lower, p.Header.Upper); err != nil {
		return nil, err
	}
	out = append(out, buf.Bytes()...)
	buf.Reset()

	for i, line := range p.LinePointers {
		if err = writeUint16(buf, line.Offset, line.Length); err != nil {
			return nil, fmt.Errorf("error at index %d, %v", i, err)
		}
	}
	out = append(out, buf.Bytes()...)
	buf.Reset()

	freeSpace := make([]byte, p.Header.Upper-p.Header.Lower)
	out = append(out, freeSpace...)

	for i := len(p.Items) - 1; i > -1; i-- {
		var bin []byte
		if bin, err = p.Items[i].Format(); err != nil {
			return nil, fmt.Errorf("error at index %d, %v", i, err)
		}
		out = append(out, bin...)
	}

	return out, nil
}

// ItemIDData is an item pointer (also called a line pointer) on a buffer page.
type ItemIDData struct {
	Offset uint16 // distance from start of page to tuple in bytes.
	Length uint16 // byte length of tuple.
}

// A HeapTuple is an in-memory representation of a tuple. In postgres, it's
// called HeapTupleData.
type HeapTuple struct {
	// Header contains metadata about the HeapTuple. It's a simpler version of
	// postgres' HeapTupleHeaderData.
	Header *struct {
		IDs        *TupleID
		Attributes []*TupleAttribute
		size       uint16 // length of entire tuple in bytes
	}
	// Data is the user data.
	Data []byte
}

// NewHeapTuple constructs a new HeapTuple.
func NewHeapTuple(rh *grokdb.RelationHeader, id TupleID, tp *grokdb.Tuple) (*HeapTuple, error) {
	tup := *tp
	if len(rh.Attributes) != len(tup) {
		return nil, fmt.Errorf(
			"mismatched lengths for attributes and tuples; len(tupAttrs) = %d, len(tup) = %d",
			len(rh.Attributes), len(tup),
		)
	}
	buf := new(bytes.Buffer)
	tupleAttributes := make([]*TupleAttribute, len(rh.Attributes))
	offset := uint16(0)
	for i, attr := range rh.Attributes {
		var err error
		switch attr.Type {
		case grokdb.Bool, grokdb.Float:
			err = binary.Write(buf, ByteOrder, tup[i])
		case grokdb.Int:
			num := uint64(tup[i].(int))
			err = binary.Write(buf, ByteOrder, num)
		case grokdb.String:
			str := tup[i].(string)
			bin := []byte(str)
			err = binary.Write(buf, ByteOrder, bin)
		default:
			err = fmt.Errorf("Cannot handle type %v", attr.Type)
		}
		if err != nil {
			return nil, err
		}
		tupleAttributes[i] = &TupleAttribute{
			ID:     attr.ID,
			Offset: offset,
		}
		if attr.Type != grokdb.String {
			offset += uint16(attributeWidthsByType[attr.Type])
		} else {
			val := tup[i].(string)
			offset += uint16(len(val))
		}
	}

	headerLength := uint16(tupleIDWidth) +
		uint16(tupleAttributeWidth*len(rh.Attributes)) +
		tupleSize

	return &HeapTuple{
		Header: &struct {
			IDs        *TupleID
			Attributes []*TupleAttribute
			size       uint16
		}{
			IDs:        &id,
			Attributes: tupleAttributes,
			size:       headerLength + offset,
		},
		Data: buf.Bytes(),
	}, nil
}

// Format returns t in binary form.
func (t HeapTuple) Format() ([]byte, error) {
	var out []byte
	buf := new(bytes.Buffer)

	if err := writeUint16(buf, t.Header.IDs.Page, t.Header.IDs.Slot); err != nil {
		return nil, err
	}
	out = append(out, buf.Bytes()...)
	buf.Reset()

	// record number of attributes before formatting attributes so we can figure
	// out where attributes end and user data begins. We're also adding a
	// placeholder byte since we need same alignment as what what we'd get if we
	// serialized the Header.size field.
	if err := writeUint16(buf, uint16(len(t.Header.Attributes))); err != nil {
		return nil, err
	}

	for _, attr := range t.Header.Attributes {
		if err := writeUint16(buf, attr.ID, attr.Offset); err != nil {
			return nil, err
		}
	}
	out = append(out, buf.Bytes()...)

	data := make([]byte, len(t.Data))
	copy(data, t.Data)
	out = append(out, data...)
	return out, nil
}

// attrLen returns the number of bytes taken up by the attribute value at index
// i. Use this method to get the width of variable length data. Use the
// AttributeWidths map for fixed width data.
func (t *HeapTuple) attrLen(i int) (length uint16, err error) {
	n := len(t.Header.Attributes)
	if i < 0 || i >= n {
		err = fmt.Errorf("index %d out of range", i)
		return
	}
	if n == 1 {
		length = t.Header.size
		return
	}
	if i == n-1 {
		headerLen := uint16(tupleIDWidth) + uint16(tupleAttributeWidth*n) + tupleSize
		length = t.Header.size - headerLen - t.Header.Attributes[i].Offset
		return
	}
	length = t.Header.Attributes[i+1].Offset - t.Header.Attributes[i].Offset
	return
}

// ToTuple converts t to a grokdb.Tuple and returns an error if it has invalid
// data.
func (t *HeapTuple) ToTuple() (out grokdb.Tuple, err error) {
	out = make(grokdb.Tuple, len(t.Header.Attributes))
	reader := bytes.NewReader(t.Data)
	var p int
	for i, a := range t.Header.Attributes {
		attr, ok := catalog.Attributes.Get(a.ID)
		if !ok {
			out = nil
			err = fmt.Errorf("unregistered attribute %v", a)
			return
		}
		width := attributeWidthsByType[attr.Type]
		switch attr.Type {
		case grokdb.String:
			n, e := t.attrLen(i)
			if e != nil {
				out = nil
				err = e
				return
			}
			val := make([]byte, n)
			if err := binary.Read(reader, ByteOrder, &val); err != nil {
				return nil, err
			}
			out[i] = string(val)
		case grokdb.Bool:
			var val bool
			if err := binary.Read(reader, ByteOrder, &val); err != nil {
				return nil, err
			}
			out[i] = val
		case grokdb.Int:
			var val int64
			if err := binary.Read(reader, ByteOrder, &val); err != nil {
				return nil, err
			}
			out[i] = int(val)
		case grokdb.Float:
			var val float64
			if err := binary.Read(reader, ByteOrder, &val); err != nil {
				return nil, err
			}
			out[i] = val
		default:
			err := fmt.Errorf("Cannot handle type %v", attr.Type)
			return nil, err
		}
		p += int(width)
	}
	return
}

// TupleID is address information for a tuple on a page.
type TupleID struct {
	// Page identifies which page has the data.
	Page uint16
	// Slot is the offset number, or index of entry in list of line pointers.
	// Where on the page is the data?
	Slot uint16
}

// TupleAttribute is metadata on a column in a HeapTuple. The ID field refers to
// an individual Attribute in the system catalog. The Offset field is the number
// of bytes between from the start of the tuple and the start of the data.
type TupleAttribute struct {
	ID     uint16
	Offset uint16
}

func writeUint16(buf *bytes.Buffer, nums ...uint16) error {
	for _, num := range nums {
		if err := binary.Write(buf, ByteOrder, num); err != nil {
			return err
		}
	}
	return nil
}

// An EOP signals End of Page, for when there is not enough room in the Page to
// insert a HeapTuple.
type EOP struct {
	pageID             uint16
	linePointersOffset uint16
	itemsOffset        uint16
	tupleLength        uint16
}

// Error outputs the error message.
func (e EOP) Error() string {
	return fmt.Sprintf("not enough room to insert HeapTuple; %#v", e)
}

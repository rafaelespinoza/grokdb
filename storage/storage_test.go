package storage

import (
	"bytes"
	"testing"

	"bitbucket.org/rafaelespinoza/grokdb"
)

func TestHeapRelationHeader(t *testing.T) {
	inputHeader := grokdb.RelationHeader{
		Name: "players",
		Attributes: []grokdb.Attribute{
			{Name: "id", Type: grokdb.Int},
			{Name: "name", Type: grokdb.String},
			{Name: "active", Type: grokdb.Bool},
			{Name: "score", Type: grokdb.Float},
		},
	}
	initCatalogs(&inputHeader)

	formattedHeader := []byte{
		0x00, 0x01, // ID
		0x07,                                     // length_of_name <uint8>
		0x04,                                     // num_attributes <uint8>
		0x70, 0x6C, 0x61, 0x79, 0x65, 0x72, 0x73, // Name <string>
		0x00, 0x01, 0x00, 0x02, 0x00, 0x03, 0x00, 0x04, // []attribute_id
	}

	hpr := HeapRelationHeader{
		RelationHeader: inputHeader,
	}
	out, err := hpr.Format()
	if err != nil {
		t.Error(err)
		return
	}
	if !bytes.Equal(out, formattedHeader) {
		t.Errorf("wrong output;\ngot      %#v\nexpected %#v", out, formattedHeader)
	}
}

func TestPage(t *testing.T) {
	inputHeader := grokdb.RelationHeader{
		Name: "foo",
		Attributes: []grokdb.Attribute{
			{Name: "id", Type: grokdb.Int},
			{Name: "name", Type: grokdb.String},
			{Name: "score", Type: grokdb.Float},
			{Name: "cool", Type: grokdb.Bool},
		},
	}
	initCatalogs(&inputHeader)

	type ExpectedPageState struct {
		header       struct{ lower, upper, relationID uint16 }
		linePointers []struct{ offset, length uint16 }
	}

	type ExpectedTupleOutput struct {
		attributes      []struct{ offset, length uint16 }
		size            uint16
		data            []byte
		formattedOutput []byte
	}

	type ExpectedOutput struct {
		page  ExpectedPageState
		tuple ExpectedTupleOutput
	}

	const (
		testPageSize = 128
		testPageID   = uint16(17185)
	)

	test := struct {
		header   *grokdb.RelationHeader
		tuples   []grokdb.Tuple
		expected []ExpectedOutput
	}{
		header: &inputHeader,
		tuples: []grokdb.Tuple{
			[]interface{}{1, "ABC", 2.5, false},
			[]interface{}{2, "DEADBEEF", 1.5, true},
		},
		expected: []ExpectedOutput{
			{
				page: ExpectedPageState{
					header: struct {
						lower, upper, relationID uint16
					}{
						10, testPageSize - (20 + 22), 1,
					},
					linePointers: []struct {
						offset, length uint16
					}{
						{testPageSize - (20 + 22), 20 + 22},
					},
				},
				tuple: ExpectedTupleOutput{
					attributes: []struct{ offset, length uint16 }{
						{offset: 0, length: 8},
						{offset: 8, length: 3},
						{offset: 11, length: 8},
						{offset: 19, length: 1},
					},
					size: 20 + 22,
					data: []byte{
						0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, // 1
						0x41, 0x42, 0x43, // "ABC"
						0x40, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 2.5
						0x00, // false
					},
					formattedOutput: []byte{
						// Header.IDs
						0x43, 0x21, 0x00, 0x00,
						// number of attributes
						0x00, 0x04,
						// Header.Attributes ([]TupleAttribute)
						0x00, 0x01, 0x00, 0x00,
						0x00, 0x02, 0x00, 0x08,
						0x00, 0x03, 0x00, 0x0B,
						0x00, 0x04, 0x00, 0x13,
						// Data []byte
						0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, // 1
						0x41, 0x42, 0x43, // "ABC"
						0x40, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 2.5
						0x00, // false
					},
				},
			},
			{
				page: ExpectedPageState{
					header: struct {
						lower, upper, relationID uint16
					}{
						14, testPageSize - (45 + (2 * 22)), 1,
					},
					linePointers: []struct{ offset, length uint16 }{
						{testPageSize - (20 + (1 * 22)), 42},
						{testPageSize - (45 + (2 * 22)), 47},
					},
				},
				tuple: ExpectedTupleOutput{
					attributes: []struct{ offset, length uint16 }{
						{offset: 0, length: 8},
						{offset: 8, length: 8},
						{offset: 16, length: 8},
						{offset: 24, length: 1},
					},
					size: 47,
					data: []byte{
						0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, // 2
						0x44, 0x45, 0x41, 0x44, 0x42, 0x45, 0x45, 0x46, // "DEADBEEF"
						0x3F, 0xF8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 1.5
						0x01, // true
					},
					formattedOutput: []byte{
						// Header.IDs
						0x43, 0x21, 0x00, 0x01,
						// number of attributes
						0x00, 0x04,
						// Header.Attributes ([]TupleAttribute)
						0x00, 0x01, 0x00, 0x00,
						0x00, 0x02, 0x00, 0x08,
						0x00, 0x03, 0x00, 0x10,
						0x00, 0x04, 0x00, 0x18,
						// Data []byte
						0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, // 2
						0x44, 0x45, 0x41, 0x44, 0x42, 0x45, 0x45, 0x46, // "DEADBEEF"
						0x3F, 0xF8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 1.5
						0x01, // true
					},
				},
			},
		},
	}

	page, err := NewPage(test.header, testPageID, testPageSize)
	if err != nil {
		t.Error(err)
		return
	}
	if page.Header.Lower != uint16(6) {
		t.Errorf(
			"expected %d for page.Header.Lower; got %d",
			uint16(6), page.Header.Lower,
		)
	}
	if page.Header.Upper != testPageSize {
		t.Errorf(
			"expected %d for page.Header.Upper; got %d",
			testPageSize, page.Header.Upper,
		)
	}
	if page.Header.RelationID != test.header.ID {
		t.Errorf(
			"expected %d for page.Header.RelationID; got %d",
			test.header.ID, page.Header.RelationID,
		)
	}

	for i, tup := range test.tuples {
		id, err := page.Put(tup)
		if err != nil { // TODO: write test to handle EOP
			t.Error(err)
		}
		if id.Page != testPageID {
			t.Errorf(
				"test %d; wrong page id; got %d, expected %d",
				i, id.Page, testPageID,
			)
		}
		expSlotID := uint16(i)
		if id.Slot != expSlotID {
			t.Errorf(
				"test %d; wrong slot id; got %d, expected %d",
				i, id.Slot, expSlotID,
			)
		}
		// test Page.Header
		if page.Header.Lower != test.expected[i].page.header.lower {
			t.Errorf(
				"test %d; expected %d for Header.Lower, got %d",
				i, test.expected[i].page.header.lower, page.Header.Lower,
			)
		}
		if page.Header.Upper != test.expected[i].page.header.upper {
			t.Errorf(
				"test %d; expected %d for Header.Upper, got %d",
				i, test.expected[i].page.header.upper, page.Header.Upper,
			)
		}
		// test Page.LinePointers
		if len(page.LinePointers) != len(test.expected[i].page.linePointers) {
			t.Errorf(
				"test %d; expected length %d for LinePointers, got %d",
				i, len(test.expected[i].page.linePointers), len(page.LinePointers),
			)
		}
		for j, line := range test.expected[i].page.linePointers {
			if page.LinePointers[j].Length != line.length {
				t.Errorf(
					"test [%d][%d]; expected length %d for LinePointer, got %d",
					i, j, line.length, page.LinePointers[j].Length,
				)
			}
			if page.LinePointers[j].Offset != line.offset {
				t.Errorf(
					"test [%d][%d]; expected offset %d for LinePointer, got %d",
					i, j, line.offset, page.LinePointers[j].Offset,
				)
			}
		}

		if page.Header.Lower != test.expected[i].page.header.lower {
			t.Errorf(
				"test %d; expected %d for Header.Lower, got %d",
				i, test.expected[i].page.header.lower, page.Header.Lower,
			)
		}
		if page.Header.Upper != test.expected[i].page.header.upper {
			t.Errorf(
				"test %d; expected %d for Header.Upper, got %d",
				i, test.expected[i].page.header.upper, page.Header.Upper,
			)
		}

		heapTuple, err := page.Get(id.Slot)
		if err != nil {
			t.Error(err)
			return
		}

		// test HeapTuple.Header.IDs
		if heapTuple.Header.IDs.Page != testPageID {
			t.Errorf(
				"test %d; wrong tuple ID page; got %d, expected %d",
				i, heapTuple.Header.IDs.Page, testPageID,
			)
		}
		if heapTuple.Header.IDs.Slot != expSlotID {
			t.Errorf(
				"test %d; wrong tuple ID slot; got %d, expected %d",
				i, heapTuple.Header.IDs.Slot, expSlotID,
			)
		}

		// test HeapTuple.Header.Attributes
		for j, attr := range heapTuple.Header.Attributes {
			expID := uint16(j + 1)
			if attr.ID != expID {
				t.Errorf(
					"test [%d][%d]; wrong ID; got %d, expected %d",
					i, j, attr.ID, expID,
				)
			}
			if attr.Offset != test.expected[i].tuple.attributes[j].offset {
				t.Errorf(
					"test [%d][%d]; wrong Offset; got %d, expected %d",
					i, j, attr.Offset, test.expected[i].tuple.attributes[j].offset,
				)
			}
			// test HeapTuple.attrLen()
			if n, e := heapTuple.attrLen(j); e != nil {
				t.Error(e)
				return
			} else if n != test.expected[i].tuple.attributes[j].length {
				t.Errorf(
					"test [%d][%d]; wrong length for attribute; got %d, expected %d",
					i, j, n, test.expected[i].tuple.attributes[j].length,
				)
			}
		}

		if heapTuple.Header.size != test.expected[i].tuple.size {
			t.Errorf(
				"test %d; got wrong length for heap tuple; got %d, expected %d",
				i, heapTuple.Header.size, test.expected[i].tuple.size,
			)
		}

		// test HeapTuple.Data
		if !bytes.Equal(heapTuple.Data, test.expected[i].tuple.data) {
			t.Errorf(
				"test %d; actual output did not equal expected. \nactual:   %x\nexpected: %x\n",
				i, heapTuple.Data, test.expected[i].tuple.data,
			)
		}

		if out, err := heapTuple.Format(); err != nil {
			t.Error(err)
		} else if !bytes.Equal(out, test.expected[i].tuple.formattedOutput) {
			t.Errorf(
				"test %d; actual output did not equal expected. \nactual:   %x\nexpected: %x\n",
				i, out, test.expected[i].tuple.formattedOutput,
			)
		}
	}

	expectedFormattedPage := []byte{
		// Header
		0x00, 0x01, 0x00, 0x0e, 0x00, 0x27,
		// []LinePointers
		0x00, 0x56, 0x00, 0x2a,
		0x00, 0x27, 0x00, 0x2f,
		// "Free Space"
		0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00,
		0x00,
		// Row[1]
		0x43, 0x21, 0x00, 0x01,
		0x00, 0x04,
		0x00, 0x01, 0x00, 0x00,
		0x00, 0x02, 0x00, 0x08,
		0x00, 0x03, 0x00, 0x10,
		0x00, 0x04, 0x00, 0x18,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02,
		0x44, 0x45, 0x41, 0x44, 0x42, 0x45, 0x45, 0x46,
		0x3F, 0xF8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x01,
		// Row[0]
		0x43, 0x21, 0x00, 0x00,
		0x00, 0x04,
		0x00, 0x01, 0x00, 0x00,
		0x00, 0x02, 0x00, 0x08,
		0x00, 0x03, 0x00, 0x0B,
		0x00, 0x04, 0x00, 0x13,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01,
		0x41, 0x42, 0x43,
		0x40, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00,
	}
	if len(expectedFormattedPage) != testPageSize {
		t.Errorf(
			"ensure length %d for expectedFormattedPage; got %d",
			testPageSize, len(expectedFormattedPage),
		)
		return
	}

	if out, err := page.Format(); err != nil {
		t.Error(err)
	} else if !bytes.Equal(out, expectedFormattedPage) {
		t.Errorf(
			"actual output did not equal expected. \nactual:   %x\nexpected: %x\n",
			out, expectedFormattedPage,
		)
	}
}

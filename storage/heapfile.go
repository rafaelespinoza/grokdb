package storage

import (
	"fmt"
	"io"
	"log"

	"bitbucket.org/rafaelespinoza/grokdb"
	"bitbucket.org/rafaelespinoza/grokdb/storage/block"
)

const pageSize = 8192 // ~8kb

// A HeapFile combines a block file with a relation header and a page.
type HeapFile struct {
	blockFile *block.File
	page      *Page
	header    *grokdb.RelationHeader
	closed    bool
}

// NewHeapFile creates a file at path and allocates a page block.
func NewHeapFile(path string, header *grokdb.RelationHeader) (out *HeapFile, err error) {
	var blockFile *block.File
	var page *Page

	if blockFile, err = block.OpenFile(path, pageSize); err != nil {
		return
	} else if blockFile.NumBlocks > 0 {
		err = fmt.Errorf("file at %s is non-empty", path)
		return
	}

	id, err := blockFile.Allocate(1)
	if err != nil {
		return
	}
	if page, err = NewPage(header, uint16(id), pageSize); err != nil {
		return
	}

	out = &HeapFile{}
	out.blockFile = blockFile
	out.page = page
	out.header = header

	return
}

// OpenHeapFile loads an existing file, parses the last block of the file and
// keeps a pointer to the page. It will return an error if there's a file error
// or parsing error.
func OpenHeapFile(path string, header *grokdb.RelationHeader) (out *HeapFile, err error) {
	var blockFile *block.File
	var page *Page

	if blockFile, err = block.OpenFile(path, pageSize); err != nil {
		return
	} else if blockFile.NumBlocks == 0 {
		err = fmt.Errorf("file at %s is empty", path)
		return
	}

	if page, err = parsePage(blockFile, blockFile.NumBlocks-1, pageSize); err != nil {
		return
	}

	out = &HeapFile{}
	out.blockFile = blockFile
	out.page = page
	out.header = header

	return
}

// BulkLoad consumes iterator's values and inserts them into pages until it
// reaches an io.EOF error. When this occurs, the file is closed. It is designed
// to hold no more than one tuple value at a time.
func (f *HeapFile) BulkLoad(iterator grokdb.Iterator) error {
	var tup grokdb.Tuple
	var err error

	for {
		if tup, err = iterator.Next(); err == io.EOF {
			return f.Close()
		} else if err != nil {
			return err
		}
		if _, err = f.Insert(tup); err != nil {
			return err
		}
	}
}

// Insert returns the id of the page that tup was inserted into. If the page is
// out of room, then it will flush that page to disk, allocate a new page for
// the file and attempt to add the tuple to the new page.
func (f *HeapFile) Insert(tup grokdb.Tuple) (tupID TupleID, err error) {
	var page *Page
	var id int32

	for {
		tupID, err = f.page.Put(tup)
		switch e := err.(type) {
		case nil:
			return
		case EOP:
			log.Println(e) // "continue"
		default:
			err = e
			return
		}

		// page is out of room. must make more room before we can insert tuple.
		if err = f.Flush(); err != nil {
			tupID = TupleID{}
			return
		}
		if id, err = f.blockFile.Allocate(1); err != nil {
			tupID = TupleID{}
			return
		}
		if page, err = NewPage(f.header, uint16(id), pageSize); err != nil {
			tupID = TupleID{}
			return
		}
		f.page = page
	}
	return
}

// Close flushes the page contents to disk before closing the underlying file.
func (f *HeapFile) Close() (err error) {
	if f.closed {
		return
	}
	defer func() {
		f.closed = true
	}()
	if err = f.Flush(); err != nil {
		return
	}
	return f.blockFile.Close()
}

// Flush writes the contents of the internal page to disk.
func (f *HeapFile) Flush() (err error) {
	out, err := f.page.Format()
	if err != nil {
		return
	}
	return f.blockFile.WriteAt(out, int32(f.page.id))
}

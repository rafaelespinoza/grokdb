package storage

import (
	"bytes"
	"encoding/binary"
	"fmt"

	"bitbucket.org/rafaelespinoza/grokdb"
	"bitbucket.org/rafaelespinoza/grokdb/catalog"
	"bitbucket.org/rafaelespinoza/grokdb/storage/block"
)

// ByteOrder is the byte ordering for encoding and decoding data.
var ByteOrder = binary.BigEndian

func parseHeader(r *bytes.Reader) (header *grokdb.RelationHeader, err error) {
	header = &grokdb.RelationHeader{}

	if err = binary.Read(r, ByteOrder, &header.ID); err != nil {
		return
	}

	var nameLen, numAttrs uint8
	if err = binary.Read(r, ByteOrder, &nameLen); err != nil {
		return
	}
	if err = binary.Read(r, ByteOrder, &numAttrs); err != nil {
		return
	}

	nameBytes := make([]byte, nameLen)
	if err = binary.Read(r, ByteOrder, nameBytes); err != nil {
		return
	}
	header.Name = string(nameBytes)

	attrIDs := make([]uint16, numAttrs)
	if err = binary.Read(r, ByteOrder, attrIDs); err != nil {
		return
	}
	header.Attributes = make([]grokdb.Attribute, numAttrs)
	for i, id := range attrIDs {
		out, found := catalog.Attributes.Get(id)
		if !found {
			err = fmt.Errorf("could not find attribute id %d", id)
			return
		}
		header.Attributes[i] = out
	}
	return
}

func parsePage(file *block.File, blockID int32, pageSize uint16) (out *Page, err error) {
	in := make([]byte, int(pageSize))
	if err = file.ReadAt(in, blockID); err != nil {
		return
	}
	reader := bytes.NewReader(in)

	var headerID uint16
	if err = binary.Read(reader, ByteOrder, &headerID); err != nil {
		return
	}
	header, found := catalog.RelationHeaders.Get(headerID)
	if !found {
		err = fmt.Errorf("header %d not found", headerID)
		return
	}

	out, err = NewPage(&header, uint16(blockID), pageSize)
	if err = binary.Read(reader, ByteOrder, &out.Header.Lower); err != nil {
		return
	}
	if err = binary.Read(reader, ByteOrder, &out.Header.Upper); err != nil {
		return
	}

	numLinePointers := int(out.Header.Lower-pageHeaderWidth) / itemIDWidth
	out.LinePointers = make([]*ItemIDData, numLinePointers)
	for i := 0; i < numLinePointers; i++ {
		lp := ItemIDData{}
		if err = binary.Read(reader, ByteOrder, &lp.Offset); err != nil {
			return
		}
		if err = binary.Read(reader, ByteOrder, &lp.Length); err != nil {
			return
		}
		out.LinePointers[i] = &lp
	}

	out.Items = make([]*HeapTuple, numLinePointers)
	for i := numLinePointers - 1; i > -1; i-- {
		linePointer := out.LinePointers[i]
		b := make([]byte, linePointer.Length)
		if _, err = reader.ReadAt(b, int64(linePointer.Offset)); err != nil {
			return
		}
		var htup *HeapTuple
		htup, err = parseHeapTuple(b, linePointer.Length)
		if err != nil {
			return
		}
		out.Items[i] = htup
	}

	return
}

func parseHeapTuple(in []byte, size uint16) (out *HeapTuple, err error) {
	reader := bytes.NewReader(in)
	headerIDs := TupleID{}
	var headerLength uint16
	if err = binary.Read(reader, ByteOrder, &headerIDs.Page); err != nil {
		out = nil
		return
	}
	if err = binary.Read(reader, ByteOrder, &headerIDs.Slot); err != nil {
		out = nil
		return
	}
	headerLength += tupleIDWidth
	var numAttrs uint16
	if err = binary.Read(reader, ByteOrder, &numAttrs); err != nil {
		out = nil
		return
	}
	headerLength += tupleSize
	headerAttributes := make([]*TupleAttribute, numAttrs)
	for i := range headerAttributes {
		attr := TupleAttribute{}
		if err = binary.Read(reader, ByteOrder, &attr.ID); err != nil {
			out = nil
			return
		}
		if err = binary.Read(reader, ByteOrder, &attr.Offset); err != nil {
			out = nil
			return
		}
		headerAttributes[i] = &attr
		headerLength += itemIDWidth
	}
	header := struct {
		IDs        *TupleID
		Attributes []*TupleAttribute
		size       uint16 // length of entire tuple in bytes
	}{
		IDs:        &headerIDs,
		Attributes: headerAttributes,
		size:       size,
	}
	out = &HeapTuple{
		Header: &header,
	}
	var p int
	data := make([]byte, size-headerLength)
	for i, tupAttr := range headerAttributes {
		fetchedAttr, ok := catalog.Attributes.Get(tupAttr.ID)
		if !ok {
			out = nil
			err = fmt.Errorf("unregistered attribute %v", tupAttr)
			return
		}

		var width uint16
		if fetchedAttr.Type != grokdb.String {
			width = uint16(attributeWidthsByType[fetchedAttr.Type])
		} else {
			width, err = out.attrLen(i)
			if err != nil {
				out = nil
				return
			}
		}
		q := p + int(width)
		if err = binary.Read(reader, ByteOrder, data[p:q]); err != nil {
			out = nil
			return
		}
		p = q
	}

	out.Data = data
	return
}

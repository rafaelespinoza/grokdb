package catalog_test

import (
	"sync"
	"testing"

	"bitbucket.org/rafaelespinoza/grokdb"
	"bitbucket.org/rafaelespinoza/grokdb/catalog"
)

func TestCatalogs(t *testing.T) {
	type createTableState struct {
		inputName   string
		inputIndex  int
		outputID    uint16
		outputError error
	}
	t.Run("concurrent access", func(t *testing.T) {
		catalog.Reboot()

		inputHeaders := []grokdb.RelationHeader{
			{
				Name: "foos",
				Attributes: []grokdb.Attribute{
					{Name: "alpha", Type: grokdb.Int},
					{Name: "bravo", Type: grokdb.String},
					{Name: "charlie", Type: grokdb.Float},
					{Name: "delta", Type: grokdb.Bool},
				},
			},
			{
				Name: "bars",
				Attributes: []grokdb.Attribute{
					{Name: "alpha", Type: grokdb.Bool},
					{Name: "bravo", Type: grokdb.Float},
					{Name: "charlie", Type: grokdb.String},
					{Name: "delta", Type: grokdb.Int},
				},
			},
		}
		states := make(chan *createTableState, len(inputHeaders))
		hold := sync.WaitGroup{}
		for ind, header := range inputHeaders {
			hold.Add(1)
			go func(h grokdb.RelationHeader, i int) {
				defer hold.Done()
				id, err := catalog.CreateTable(h.Name, h.Attributes...)
				states <- &createTableState{
					inputName:   h.Name,
					inputIndex:  i,
					outputID:    id,
					outputError: err,
				}
			}(header, ind)
		}
		hold.Wait()

		expectedResults := []grokdb.RelationHeader{
			{
				Name: "foos",
				Attributes: []grokdb.Attribute{
					{Name: "alpha", Type: grokdb.Int},
					{Name: "bravo", Type: grokdb.String},
					{Name: "charlie", Type: grokdb.Float},
					{Name: "delta", Type: grokdb.Bool},
				},
			},
			{
				Name: "bars",
				Attributes: []grokdb.Attribute{
					{Name: "alpha", Type: grokdb.Bool},
					{Name: "bravo", Type: grokdb.Float},
					{Name: "charlie", Type: grokdb.String},
					{Name: "delta", Type: grokdb.Int},
				},
			},
		}

		for i := 0; i < cap(states); i++ {
			result := <-states
			if result.outputError != nil {
				t.Error(result.outputError)
				continue
			}
			relationKey := uint16(result.inputIndex + 1)
			output, found := catalog.RelationHeaders.Get(relationKey)
			if !found {
				t.Errorf(
					"relation %s id %d not found",
					inputHeaders[i].Name, result.outputID,
				)
				continue
			}
			if output.Name != expectedResults[i].Name {
				t.Errorf(
					"test %d; expected relation name %s, got %s",
					i, output.Name, expectedResults[i].Name,
				)
			}
			numAttrs := uint16(len(expectedResults[i].Attributes))
			for j, expAttr := range expectedResults[i].Attributes {
				// Test modulus of actual ID because there's no guaranteed order
				// when concurrently inserting relations. However, inserting the
				// relation's attributes should be in the same order relative to
				// the other attributes in same relation.
				actualID := output.Attributes[j].ID
				modID := actualID % numAttrs
				if modID == 0 {
					modID = uint16(j + 1)
				}
				expID := uint16(j + 1)
				if modID != expID {
					t.Errorf(
						"test [%d][%d]; expected attribute ID %d, got %d",
						i, j, expID, modID,
					)
				}
				if output.Attributes[j].Name != expAttr.Name {
					t.Errorf(
						"test [%d][%d]; expected attribute Name %s, got %s",
						i, j, output.Attributes[j].Name, expAttr.Name,
					)
				}
				if output.Attributes[j].Type != expAttr.Type {
					t.Errorf(
						"test [%d][%d]; expected attribute Type %s, got %s",
						i, j, output.Attributes[j].Type, expAttr.Type,
					)
				}

				// should also be able to find it in TheAttributeCatalog.
				attr, found := catalog.Attributes.Get(actualID)
				if !found {
					t.Errorf(
						"test [%d][%d]; expected to find attribute %d in catalog",
						i, j, actualID,
					)
					continue
				}
				if attr.Name != expAttr.Name {
					t.Errorf(
						"test [%d][%d]; expected attribute Name %s, got %s",
						i, j, expAttr.Name, attr.Name,
					)
				}
				if attr.Type != expAttr.Type {
					t.Errorf(
						"test [%d][%d]; expected attribute Type %s, got %s",
						i, j, expAttr.Type, attr.Type,
					)
				}
			}
		}
	})

	t.Run("duplicate relations", func(t *testing.T) {
		t.Skipf("restore when there is some kind of uniqueness constraint for Name")
		catalog.Reboot()

		inputHeaders := []grokdb.RelationHeader{
			{
				Name:       "bazs",
				Attributes: []grokdb.Attribute{{Name: "a", Type: grokdb.Int}},
			},
			{
				Name:       "bazs",
				Attributes: []grokdb.Attribute{{Name: "b", Type: grokdb.Bool}},
			},
		}
		states := make(chan *createTableState, len(inputHeaders))
		hold := sync.WaitGroup{}
		for ind, header := range inputHeaders {
			hold.Add(1)
			go func(h grokdb.RelationHeader, i int) {
				defer hold.Done()
				id, err := catalog.CreateTable(h.Name, h.Attributes...)
				states <- &createTableState{
					inputName: h.Name, inputIndex: i,
					outputID: id, outputError: err,
				}
			}(header, ind)
		}
		hold.Wait()

		// expect success
		okResult := <-states
		if okResult.outputError != nil {
			t.Errorf("did not expect error, got %v", okResult.outputError)
		} else if okResult.outputID == 0 {
			t.Errorf("expected ID, got %v", okResult.outputID)
		}
		if _, found := catalog.RelationHeaders.Get(okResult.outputID); !found {
			t.Errorf(
				"expected to find relation %s w/ id %d",
				okResult.inputName, okResult.outputID,
			)
		}

		// expect errors
		badResult := <-states
		if badResult.outputError == nil {
			t.Errorf("expected error, got %v", badResult.outputError)
		}
	})
}

package catalog

import (
	"fmt"
	"sync"

	"bitbucket.org/rafaelespinoza/grokdb"
)

// These are the globally-available system catalogs. They are safe for
// concurrent usage between multiple goroutines.
var (
	RelationHeaders *RelationHeaderCatalog
	Attributes      *AttributeCatalog
)

func init() {
	Reboot()
}

// Reboot initializes all catalogs. It's called once in this package's init
// function. Callers should only use this function for test setup.
func Reboot() {
	RelationHeaders = &RelationHeaderCatalog{catalog: newCatalog()}
	Attributes = &AttributeCatalog{catalog: newCatalog()}
}

// catalog is a base for exported *Catalog types.
type catalog struct {
	maxID uint16
	data  *sync.Map
	wait  chan bool
}

func newCatalog() *catalog {
	return &catalog{
		data: new(sync.Map),
		wait: make(chan bool, 1), // helps limit writes to 1 at a time
	}
}

// RelationHeaderCatalog is the system catalog for relation headers. It is
// globally available as RelationHeaders and is safe for concurrent usage by
// multiple goroutines.
type RelationHeaderCatalog struct {
	*catalog
}

// Get retrieves a copy of the relation header at key. The return value for
// found indicates whether or not there's a match.
func (c *RelationHeaderCatalog) Get(key uint16) (rel grokdb.RelationHeader, found bool) {
	out, found := c.data.Load(key)
	if !found {
		return
	}
	rel = *out.(*grokdb.RelationHeader)
	return
}

// CreateTable creates a Relation out of the inputs, registers the relation and
// each attribute in the catalogs. An error is returned if any of the attributes
// are already registered.
func CreateTable(name string, attributes ...grokdb.Attribute) (uint16, error) {
	return RelationHeaders.put(name, attributes)
}

// put does the work in creating a relation. It is not exported to make DDL
// operations syntactically-sugary.
func (c *RelationHeaderCatalog) put(name string, attributes []grokdb.Attribute) (id uint16, err error) {
	c.wait <- true
	defer func() { <-c.wait }()

	for i, attr := range attributes {
		if attr.ID != 0 {
			err = fmt.Errorf(
				"attribute %s already registered with id %d",
				attr.Name, attr.ID,
			)
			return
		}

		var attrID uint16
		if attrID, err = Attributes.Put(attr); err != nil {
			return
		}
		attributes[i].ID = attrID
	}

	c.maxID++
	id = c.maxID
	c.data.Store(id, &grokdb.RelationHeader{
		Name:       name,
		Attributes: attributes,
		ID:         id,
	})

	return
}

// AttributeCatalog is the system catalog for attributes. It is globally
// available as Attributes and is safe for concurrent usage by multiple
// goroutines.
type AttributeCatalog struct {
	*catalog
}

// Get retrieves a copy of the attribute at key. The return value for found
// indicates whether or not there's a matching attribute.
func (c *AttributeCatalog) Get(key uint16) (attr grokdb.Attribute, found bool) {
	out, found := c.data.Load(key)
	if !found {
		return
	}
	attr = *out.(*grokdb.Attribute)
	return
}

// Put adds an Attribute to the catalog. It will assign an id to the Attribute
// that is saved in the internal data field. Use the return id to update the
// caller's Attribute value or call Get to re-assign the caller.
func (c *AttributeCatalog) Put(attr grokdb.Attribute) (id uint16, err error) {
	c.wait <- true
	defer func() { <-c.wait }()

	c.maxID++
	id = c.maxID
	attr.ID = id
	c.data.Store(id, &attr)

	return
}

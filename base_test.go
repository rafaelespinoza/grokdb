package grokdb

import (
	"fmt"
	"reflect"
	"testing"
)

func TestTupleClone(t *testing.T) {
	original := Tuple([]interface{}{"alpha", "bravo", "charlie", "delta"})
	result := original.Clone()
	result[0] = "echo"
	if original[0] == result[0] {
		t.Errorf("Changes to result should not mutate original")
	}
}

func TestTupleMap(t *testing.T) {
	tests := []struct {
		tuple          Tuple
		cols           []int
		expectedTuple  Tuple
		expectingError bool
	}{
		{
			Tuple([]interface{}{"alpha", "bravo", "charlie", "delta"}),
			[]int{1, 3},
			Tuple([]interface{}{"bravo", "delta"}),
			false,
		},
		{
			Tuple([]interface{}{"alpha", "bravo", "charlie", "delta"}),
			[]int{-1},
			nil,
			true,
		},
		{
			Tuple([]interface{}{"alpha", "bravo", "charlie", "delta"}),
			[]int{4},
			nil,
			true,
		},
		{
			Tuple([]interface{}{"alpha", "bravo", "charlie", "delta"}),
			[]int{3, 2, 1, 0},
			Tuple([]interface{}{"delta", "charlie", "bravo", "alpha"}),
			false,
		},
		{
			Tuple([]interface{}{"alpha", "bravo", "charlie", "delta"}),
			[]int{0, 1, 2, 3, 2, 1, 0},
			nil,
			true,
		},
	}

	for i, test := range tests {
		actualTuple, err := test.tuple.Map(test.cols)
		errPresent := err != nil
		if errPresent && !test.expectingError {
			t.Errorf("Not expecting an error but got one %v. Test %d", err, i)
		}
		if !errPresent && test.expectingError {
			t.Errorf("Expecting an error but got none. Test %d", i)
		}

		if !reflect.DeepEqual(actualTuple, test.expectedTuple) {
			t.Errorf("Actual tuple did not equal expected tuple. Test %d", i)
			fmt.Printf("actual:		 	%v\n", actualTuple)
			fmt.Printf("expected: 		%v\n", test.expectedTuple)
		}
	}

	// test deep copy
	original := Tuple([]interface{}{"alpha", "bravo", "charlie", "delta"})
	result, _ := original.Map([]int{1, 3})
	result[0] = "echo"
	if original[1] == result[0] {
		t.Errorf("Changes to result should not mutate original")
	}
}

package grokdb

import (
	"fmt"
	"io"
)

// A Type describes attribute data.
type Type int8

// These are the supported data types.
const (
	Undefined Type = iota
	Bool
	String
	Int
	Float
)

func (t Type) String() string {
	types := [...]string{
		"Undefined",
		"Bool",
		"String",
		"Int",
		"Float",
	}

	if t < Undefined || t > Float {
		return "Undefined"
	}

	return types[t]
}

// A Relation is a table, including the metadata and the selected tuples (rows).
type Relation struct {
	RelationHeader
	Tuples []Tuple
}

// A RelationHeader is metadata for a database table. Its ID field corresponds
// to a unique identifier in the system catalog.
type RelationHeader struct {
	Name       string
	ID         uint16
	Attributes []Attribute
}

// An Attribute is a column for a relation, or table. Its ID field corresponds
// to a unique identifier in the system catalog.
type Attribute struct {
	Name string
	ID   uint16
	Type Type
}

// A Tuple is a database row.
type Tuple []interface{}

// Clone returns a deep copy of t.
func (t Tuple) Clone() Tuple {
	u := make(Tuple, len(t))
	copy(u, t)
	return u
}

// Map extracts the the values of t at indices specified in cols. An error is
// returned when any of the inputs is out of the bounds of t.
func (t Tuple) Map(cols []int) (Tuple, error) {
	tupLen := len(t)
	if len(cols) > tupLen {
		err := fmt.Errorf("Length of input cols (%d) must be <= tuple length (%d)", len(cols), tupLen)
		return nil, err
	}

	res := make(Tuple, len(cols))
	for i, col := range cols {
		if col < 0 || col >= tupLen {
			err := fmt.Errorf("Column index (%d) out of range", col)
			return nil, err
		}
		res[i] = t[col]
	}
	return res, nil
}

// An Iterator is a query executor node that repeatedly visits tuples in its
// input stream until it reaches the end or some other condition is met. They
// should be designed so one iterator can be the input of another.
type Iterator interface {
	Next() (Tuple, error)
	Reset() error
	Close() error
}

// CollectAll iterates through all of the tuples until it either reaches the end
// or it encounters some other error. If it reaches the end, then the result is
// the tuple collection and an io.EOF error. Any other kind of error results in
// an empty collection and the error.
func CollectAll(iterator Iterator) ([]Tuple, error) {
	var results []Tuple
	for tup, err := iterator.Next(); err != io.EOF; tup, err = iterator.Next() {
		if err != nil {
			return nil, err
		}
		results = append(results, tup)
	}

	if len(results) == 0 {
		return nil, io.EOF
	}
	return results, nil
}

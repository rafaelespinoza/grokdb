GO := /usr/bin/env go
PKGS := . ./catalog ./executor ./storage/...

test:
	for pkg in $(PKGS); do $(GO) test $$pkg ; done

test-verbose:
	for pkg in $(PKGS); do $(GO) test -v $$pkg ; done
